﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class EmployeesForm : Form
    {
        public EmployeesForm()
        {
            InitializeComponent();
        }

        private void EmployeesForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.Сотрудник". При необходимости она может быть перемещена или удалена.
            this.сотрудникTableAdapter.Fill(this.tuberStatisticDBDataSet.Сотрудник);

        }
        
        private void okButton_Click(object sender, EventArgs e)
        {
            this.сотрудникTableAdapter.Update(this.tuberStatisticDBDataSet.Сотрудник);
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "Внимание!", MessageBoxButtons.YesNo);

            if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                this.okButton_Click(sender, e);
            else
                Close();
        }
    }
}
