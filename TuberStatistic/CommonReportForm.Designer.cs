﻿namespace TuberStatistic
{
    partial class CommonReportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.quarterUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.yearUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.формаTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter();
            this.формаПостоянныхTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter();
            this.empComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.empСheckBox = new System.Windows.Forms.CheckBox();
            this.сводныйОтчетПТРTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СводныйОтчетПТРTableAdapter();
            this.сотрудникTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СотрудникTableAdapter();
            this.tuberStatisticDBDataSet = new TuberStatistic.TuberStatisticDBDataSet();
            this.сотрудникBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Квартал";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(0, 91);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(310, 47);
            this.panel1.TabIndex = 32;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(145, 12);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.ForeColor = System.Drawing.Color.Black;
            this.okButton.Location = new System.Drawing.Point(226, 12);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 31;
            this.label1.Text = "Год";
            // 
            // quarterUpDown1
            // 
            this.quarterUpDown1.Location = new System.Drawing.Point(81, 38);
            this.quarterUpDown1.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.quarterUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quarterUpDown1.Name = "quarterUpDown1";
            this.quarterUpDown1.Size = new System.Drawing.Size(220, 20);
            this.quarterUpDown1.TabIndex = 30;
            this.quarterUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // yearUpDown1
            // 
            this.yearUpDown1.Location = new System.Drawing.Point(81, 12);
            this.yearUpDown1.Name = "yearUpDown1";
            this.yearUpDown1.Size = new System.Drawing.Size(220, 20);
            this.yearUpDown1.TabIndex = 29;
            // 
            // формаTableAdapter1
            // 
            this.формаTableAdapter1.ClearBeforeFill = true;
            // 
            // формаПостоянныхTableAdapter1
            // 
            this.формаПостоянныхTableAdapter1.ClearBeforeFill = true;
            // 
            // empComboBox
            // 
            this.empComboBox.DataSource = this.сотрудникBindingSource;
            this.empComboBox.DisplayMember = "ФИО";
            this.empComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.empComboBox.FormattingEnabled = true;
            this.empComboBox.Location = new System.Drawing.Point(81, 64);
            this.empComboBox.Name = "empComboBox";
            this.empComboBox.Size = new System.Drawing.Size(202, 21);
            this.empComboBox.TabIndex = 34;
            this.empComboBox.ValueMember = "Код";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 35;
            this.label3.Text = "Сотрудник";
            // 
            // empСheckBox
            // 
            this.empСheckBox.AutoSize = true;
            this.empСheckBox.Location = new System.Drawing.Point(286, 67);
            this.empСheckBox.Name = "empСheckBox";
            this.empСheckBox.Size = new System.Drawing.Size(15, 14);
            this.empСheckBox.TabIndex = 36;
            this.empСheckBox.UseVisualStyleBackColor = true;
            this.empСheckBox.CheckedChanged += new System.EventHandler(this.empСheckBox_CheckedChanged);
            // 
            // сводныйОтчетПТРTableAdapter1
            // 
            this.сводныйОтчетПТРTableAdapter1.ClearBeforeFill = true;
            // 
            // сотрудникTableAdapter1
            // 
            this.сотрудникTableAdapter1.ClearBeforeFill = true;
            // 
            // tuberStatisticDBDataSet
            // 
            this.tuberStatisticDBDataSet.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // сотрудникBindingSource
            // 
            this.сотрудникBindingSource.DataMember = "Сотрудник";
            this.сотрудникBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // CommonReportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 141);
            this.Controls.Add(this.empСheckBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.empComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.quarterUpDown1);
            this.Controls.Add(this.yearUpDown1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "CommonReportForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ЦОУ ПТР";
            this.Load += new System.EventHandler(this.CommonReportForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.NumericUpDown quarterUpDown1;
        public System.Windows.Forms.NumericUpDown yearUpDown1;
        private TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter формаTableAdapter1;
        private TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter формаПостоянныхTableAdapter1;
        private TuberStatisticDBDataSetTableAdapters.СводныйОтчетПТРTableAdapter сводныйОтчетПТРTableAdapter1;
        private System.Windows.Forms.ComboBox empComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox empСheckBox;
        private System.Windows.Forms.BindingSource сотрудникBindingSource;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet;
        private TuberStatisticDBDataSetTableAdapters.СотрудникTableAdapter сотрудникTableAdapter1;
    }
}