﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class FilterDataForm : Form
    {
        public FilterDataForm()
        {
            InitializeComponent();
        }

        private void FilterDataForm_Load(object sender, EventArgs e)
        {
            // Предполагаем, что поиск по годам будет нужен всегда
            yearCheckBox.Checked = true;
            // Устанавливаем стартовые значения на основе максимума и минимума дат форм из БД
            int minYear = (int)формаTableAdapter1.GetFirstYear();
            int maxYear = (int)формаTableAdapter1.GetLastYear();
           
            yearUpDown1.Minimum = minYear;
            yearUpDown1.Maximum = maxYear;
            yearUpDown1.Value = minYear;

            yearUpDown2.Minimum = minYear;
            yearUpDown2.Maximum = maxYear;
            yearUpDown2.Value = maxYear;

            // А вот по кварталам и территориям - только при необходимости
            quarterCheckBox.Checked = true;
            quarterCheckBox.Checked = false;
            territoryCheckBox.Checked = true;
            territoryCheckBox.Checked = false;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void quarterCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            quarterUpDown1.Enabled = quarterCheckBox.Checked;
            quarterUpDown2.Enabled = quarterCheckBox.Checked;
        }

        private void yearCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            yearUpDown1.Enabled = yearCheckBox.Checked;
            yearUpDown2.Enabled = yearCheckBox.Checked;
        }

        private void territoryCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            territoryComboBox.Enabled = territoryCheckBox.Checked;
            if (territoryComboBox.Enabled)
                // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.СтруктурнаяЕдиница". При необходимости она может быть перемещена или удалена.
                this.структурнаяЕдиницаTableAdapter.Fill(this.tuberStatisticDBDataSet.СтруктурнаяЕдиница);
            else
                territoryComboBox.SelectedValue = -1;
        }
    }
}
