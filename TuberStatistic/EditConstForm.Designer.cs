﻿namespace TuberStatistic
{
    partial class EditConstForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.yearUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.quarterUpDown = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.кодDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.структурнаяЕдиницаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tuberStatisticDBDataSet = new TuberStatistic.TuberStatisticDBDataSet();
            this.населениеВзрослоеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.населениеПодросткиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.населениеДетиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.бациллярныеБольныеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.постоянныеТерриторияBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.постоянныеТерриторияTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ПостоянныеТерриторияTableAdapter();
            this.структурнаяЕдиницаTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter();
            this.формаПостоянныхTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.постоянныеТерриторияBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(0, 565);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(1056, 49);
            this.panel1.TabIndex = 11;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(887, 16);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.ForeColor = System.Drawing.Color.Black;
            this.okButton.Location = new System.Drawing.Point(968, 16);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // yearUpDown
            // 
            this.yearUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yearUpDown.Location = new System.Drawing.Point(979, 12);
            this.yearUpDown.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.yearUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.yearUpDown.Name = "yearUpDown";
            this.yearUpDown.Size = new System.Drawing.Size(68, 20);
            this.yearUpDown.TabIndex = 15;
            this.yearUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(812, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Квартал";
            // 
            // quarterUpDown
            // 
            this.quarterUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quarterUpDown.Location = new System.Drawing.Point(867, 12);
            this.quarterUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.quarterUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quarterUpDown.Name = "quarterUpDown";
            this.quarterUpDown.Size = new System.Drawing.Size(68, 20);
            this.quarterUpDown.TabIndex = 13;
            this.quarterUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(941, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Год";
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодDataGridViewTextBoxColumn,
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn,
            this.населениеВзрослоеDataGridViewTextBoxColumn,
            this.населениеПодросткиDataGridViewTextBoxColumn,
            this.населениеДетиDataGridViewTextBoxColumn,
            this.бациллярныеБольныеDataGridViewTextBoxColumn,
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn,
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn,
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.постоянныеТерриторияBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, 45);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1056, 514);
            this.dataGridView1.TabIndex = 16;
            // 
            // кодDataGridViewTextBoxColumn
            // 
            this.кодDataGridViewTextBoxColumn.DataPropertyName = "Код";
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.кодDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle1;
            this.кодDataGridViewTextBoxColumn.HeaderText = "Код";
            this.кодDataGridViewTextBoxColumn.Name = "кодDataGridViewTextBoxColumn";
            this.кодDataGridViewTextBoxColumn.Width = 80;
            // 
            // кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn
            // 
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DataPropertyName = "Код_СтруктурнаяЕдиница";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DataSource = this.структурнаяЕдиницаBindingSource;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.HeaderText = "Территория";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.Name = "кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.ValueMember = "Код";
            // 
            // структурнаяЕдиницаBindingSource
            // 
            this.структурнаяЕдиницаBindingSource.DataMember = "СтруктурнаяЕдиница";
            this.структурнаяЕдиницаBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // tuberStatisticDBDataSet
            // 
            this.tuberStatisticDBDataSet.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // населениеВзрослоеDataGridViewTextBoxColumn
            // 
            this.населениеВзрослоеDataGridViewTextBoxColumn.DataPropertyName = "НаселениеВзрослое";
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.населениеВзрослоеDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
            this.населениеВзрослоеDataGridViewTextBoxColumn.HeaderText = "Население (Взрослые)";
            this.населениеВзрослоеDataGridViewTextBoxColumn.Name = "населениеВзрослоеDataGridViewTextBoxColumn";
            // 
            // населениеПодросткиDataGridViewTextBoxColumn
            // 
            this.населениеПодросткиDataGridViewTextBoxColumn.DataPropertyName = "НаселениеПодростки";
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.населениеПодросткиDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
            this.населениеПодросткиDataGridViewTextBoxColumn.HeaderText = "Население (Подростки)";
            this.населениеПодросткиDataGridViewTextBoxColumn.Name = "населениеПодросткиDataGridViewTextBoxColumn";
            // 
            // населениеДетиDataGridViewTextBoxColumn
            // 
            this.населениеДетиDataGridViewTextBoxColumn.DataPropertyName = "НаселениеДети";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.населениеДетиDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
            this.населениеДетиDataGridViewTextBoxColumn.HeaderText = "Население (Дети)";
            this.населениеДетиDataGridViewTextBoxColumn.Name = "населениеДетиDataGridViewTextBoxColumn";
            // 
            // бациллярныеБольныеDataGridViewTextBoxColumn
            // 
            this.бациллярныеБольныеDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.бациллярныеБольныеDataGridViewTextBoxColumn.DataPropertyName = "БациллярныеБольные";
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.бациллярныеБольныеDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
            this.бациллярныеБольныеDataGridViewTextBoxColumn.HeaderText = "Бациллярные больные на учете";
            this.бациллярныеБольныеDataGridViewTextBoxColumn.Name = "бациллярныеБольныеDataGridViewTextBoxColumn";
            // 
            // бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn
            // 
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.DataPropertyName = "БацилБольныеВВ_ПрошлыйГод";
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.HeaderText = "Бациллярные больные (в/в, прошлый год)";
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.Name = "бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn";
            this.бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn.Width = 120;
            // 
            // больныеТуберОргДыханияDataGridViewTextBoxColumn
            // 
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.DataPropertyName = "БольныеТуберОргДыхания";
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.HeaderText = "Активные больные туб. органов дыхания";
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.Name = "больныеТуберОргДыханияDataGridViewTextBoxColumn";
            this.больныеТуберОргДыханияDataGridViewTextBoxColumn.Width = 120;
            // 
            // больныеТуберВнелегочныйDataGridViewTextBoxColumn
            // 
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn.DataPropertyName = "БольныеТуберВнелегочный";
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn.HeaderText = "Активные больные внелегочным туб.";
            this.больныеТуберВнелегочныйDataGridViewTextBoxColumn.Name = "больныеТуберВнелегочныйDataGridViewTextBoxColumn";
            // 
            // постоянныеТерриторияBindingSource
            // 
            this.постоянныеТерриторияBindingSource.DataMember = "ПостоянныеТерритория";
            this.постоянныеТерриторияBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // постоянныеТерриторияTableAdapter
            // 
            this.постоянныеТерриторияTableAdapter.ClearBeforeFill = true;
            // 
            // структурнаяЕдиницаTableAdapter
            // 
            this.структурнаяЕдиницаTableAdapter.ClearBeforeFill = true;
            // 
            // формаПостоянныхTableAdapter1
            // 
            this.формаПостоянныхTableAdapter1.ClearBeforeFill = true;
            // 
            // EditConstForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 615);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.yearUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.quarterUpDown);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Name = "EditConstForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Постоянные значения";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.EditConstForm_FormClosing);
            this.Load += new System.EventHandler(this.EditCostForm_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.постоянныеТерриторияBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.NumericUpDown yearUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown quarterUpDown;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet;
        private System.Windows.Forms.BindingSource постоянныеТерриторияBindingSource;
        private TuberStatisticDBDataSetTableAdapters.ПостоянныеТерриторияTableAdapter постоянныеТерриторияTableAdapter;
        private System.Windows.Forms.BindingSource структурнаяЕдиницаBindingSource;
        private TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter структурнаяЕдиницаTableAdapter;
        private TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter формаПостоянныхTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn населениеВзрослоеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn населениеПодросткиDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn населениеДетиDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn бациллярныеБольныеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn бацилБольныеВВПрошлыйГодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn больныеТуберОргДыханияDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn больныеТуберВнелегочныйDataGridViewTextBoxColumn;
    }
}