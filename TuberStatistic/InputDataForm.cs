﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class InputDataForm : Form
    {
        
        public InputDataForm()
        {
            IdForm = 0;
            InitializeComponent();
        }

        public InputDataForm(int FormId)
        {
            InitializeComponent();
            IdForm = FormId;
        }

        protected int IdForm;
        protected bool IsNewForm = false;
        protected bool IsOKBtn = false;

        private void InputDataForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.Показатель". При необходимости она может быть перемещена или удалена.
            this.показательTableAdapter.Fill(this.tuberStatisticDBDataSet.Показатель);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.СтруктурнаяЕдиница". При необходимости она может быть перемещена или удалена.
            this.структурнаяЕдиницаTableAdapter.Fill(this.tuberStatisticDBDataSet.СтруктурнаяЕдиница);

            // Если вводим новые данные
            if (IdForm == -1)
            {
                int? currentYear = формаTableAdapter1.GetLastYear();
                int curYear;
                int curQuarter;

                DateTime dt = DateTime.Now;
                curYear = dt.Year;

                if (dt.Month <= 3)
                    curQuarter = 1;
                else
                    if (dt.Month <= 6)
                        curQuarter = 2;
                    else
                        if (dt.Month <= 9)
                            curQuarter = 3;
                        else curQuarter = 4;
                             
                формаTableAdapter1.InsertQueryEmpty(curQuarter,curYear, структурнаяЕдиницаTableAdapter.GetFirstTerritory());

                IdForm = (int)формаTableAdapter1.NextCode();

                оперативныйУчетTableAdapter.InsertQuery(IdForm);
                IsNewForm = true;
            }

            // Выводим значения показателей для формы
            this.оперативныйУчетTableAdapter.FillIndFormBy(this.tuberStatisticDBDataSet.ОперативныйУчет, (int)IdForm);

            // Загружаем данные самой формы
            DataTable fm = формаTableAdapter1.GetDataByCode(IdForm);

            quarterUpDown.Value = fm.Rows[0].Field<int>("Квартал");
            yearUpDown.Value = fm.Rows[0].Field<int>("Год");
            territoryComboBox.SelectedValue = fm.Rows[0].Field<int>("Код_СтруктурнаяЕдиница");

            try
            {
                // Штатных должностей всего, фтизиатры
                tbStFAll.Text = fm.Rows[0].Field<double>("ШтатныхДолжностейВсегоФ").ToString();
                tbStFAll.Text = tbStFAll.Text == "" ? "0" : tbStFAll.Text;
                
                // Штатных должностей всего, детские фтизиатры
                tbStCFAll.Text = fm.Rows[0].Field<double>("ШтатныхДолжностейВсегоФД").ToString();
                tbStCFAll.Text = tbStCFAll.Text == "" ? "0" : tbStCFAll.Text;

                // Штатных должностей занято, фтизиатры
                tbStFE.Text = fm.Rows[0].Field<double>("ШтатныхДолжностейЗанятоФ").ToString();
                tbStFE.Text = tbStFE.Text == "" ? "0" : tbStFE.Text;
                
                // Штатных должностей занято, фтизиатры детские
                tbStCFE.Text = fm.Rows[0].Field<double>("ШтатныхДолжностейЗанятоФД").ToString();
                tbStCFE.Text = tbStCFE.Text == "" ? "0" : tbStCFE.Text;

                // Физических лиц, фтизиатры
                tbFLF.Text = fm.Rows[0].Field<double>("ФизическихЛицФ").ToString();
                tbFLF.Text = tbFLF.Text == "" ? "0" : tbFLF.Text;

                // Физическиех лиц, фтизиатры детские
                tbFLCF.Text = fm.Rows[0].Field<double>("ФизическихЛицФД").ToString();
                tbFLCF.Text = tbFLCF.Text == "" ? "0" : tbFLCF.Text;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте правильность введенных Вами данных!");
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            IsOKBtn = true; // Согласны сохранить форму

            try
            {
                // Обновляем данные формы
                формаTableAdapter1.UpdateFormFields((int)quarterUpDown.Value, (int)yearUpDown.Value, (int)territoryComboBox.SelectedValue,
                    double.Parse(tbStFAll.Text), double.Parse(tbStFE.Text), double.Parse(tbStCFAll.Text),
                    double.Parse(tbStCFE.Text), double.Parse(tbFLF.Text), double.Parse(tbFLCF.Text), (int)IdForm);

                // Обновляем значения показателей
                оперативныйУчетTableAdapter.Update(this.tuberStatisticDBDataSet.ОперативныйУчет);

                // Закрываем форму
                Close();
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте правильность введенных Вами данных!");
                
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (IsNewForm)
            {
                оперативныйУчетTableAdapter.DeleteQuery(IdForm);
                формаTableAdapter1.DeleteQuery(IdForm);
            }
            Close();
        }

        private void InputDataForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Если выходим по нажатию "ОК", проверка не нужна
            if (!IsOKBtn)
            {
                // 
                string question = IsNewForm ? "Данные введенной формы будут потеряны. Вернуться к вводу данных?" : "Все несохраненные изменения будут потеряны! Вернуться к вводу данных?";

                DialogResult dialogResult = MessageBox.Show(question, "Внимание!", MessageBoxButtons.YesNo);

                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    // Теперь цикл проверок пойдет заново

                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = false;
            }
        }
    }
}
