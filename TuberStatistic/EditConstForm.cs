﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class EditConstForm : Form
    {
        public EditConstForm()
        {
            InitializeComponent();
            IdForm = -1;
        }

        public EditConstForm(int FormId)
        {
            InitializeComponent();
            IdForm = FormId;
        }
        
        public int IdForm;
        bool IsNewForm = false;
        public bool IsOKBtn = false;

        private void EditCostForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.СтруктурнаяЕдиница". При необходимости она может быть перемещена или удалена.
            this.структурнаяЕдиницаTableAdapter.Fill(this.tuberStatisticDBDataSet.СтруктурнаяЕдиница);
            
            if (IdForm == -1)
            {
                yearUpDown.Value = DateTime.Now.Year;
                quarterUpDown.Value = DateTime.Now.Month < 6 ? 1 : 4;

                формаПостоянныхTableAdapter1.InsertEmptyConstForm((int)yearUpDown.Value, (int)quarterUpDown.Value);

                IdForm = (int)формаПостоянныхTableAdapter1.GetNextCode();

                this.постоянныеТерриторияTableAdapter.InsertEmptyData(IdForm);
                
                IsNewForm = true;
            }

            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.ПостоянныеТерритория". При необходимости она может быть перемещена или удалена.
            this.постоянныеТерриторияTableAdapter.FillByIdForm(tuberStatisticDBDataSet.ПостоянныеТерритория, IdForm);
            
            // Теперь загружаем данные самой формы
            DataTable dt = this.формаПостоянныхTableAdapter1.GetDataByConstFormId(IdForm);

            yearUpDown.Value = dt.Rows[0].Field<int>("Год");
            quarterUpDown.Value = dt.Rows[0].Field<int>("Квартал");

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (IsNewForm)
            {
                this.постоянныеТерриторияTableAdapter.DeleteFormConsts((int)IdForm);
                this.формаПостоянныхTableAdapter1.DeleteConstForm(IdForm);
            }

            Close();
        }

        private void EditConstForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Если выходим по нажатию "ОК", проверка не нужна
            if (!IsOKBtn)
            {
                // 
                string question = IsNewForm ? "Данные введенной формы будут потеряны. Вернуться к вводу данных?" : "Все несохраненные изменения будут потеряны! Вернуться к вводу данных?";

                DialogResult dialogResult = MessageBox.Show(question, "Внимание!", MessageBoxButtons.YesNo);

                if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            try
            {
                // Сохраняем изменения показателей
                this.постоянныеТерриторияTableAdapter.Update(tuberStatisticDBDataSet.ПостоянныеТерритория);
                // Сохраняем изменения формы
                this.формаПостоянныхTableAdapter1.UpdateConstFormData((int)yearUpDown.Value, (int)quarterUpDown.Value, IdForm);
                //
                IsOKBtn = true;

                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Проверьте правильность введенных Вами данных!");
            }
        }
    }
}
