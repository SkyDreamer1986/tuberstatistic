﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace TuberStatistic
{
    public partial class MainForm : Form
    {
        // Параметры фильтрации
        bool filterDataOn;
        int? yearStart, yearEnd;
        int? quarterStart, quarterEnd;
        int? territoryId;

        bool filterConstOn;
        int? yearConstStart, yearConstEnd;
        int? quarterConstStart, quarterConstEnd;

        Excel.Application excelapp;
        Excel.Sheets excelsheets;
        Excel.Worksheet excelworksheet;
        Excel.Range excelcells;
        Excel.Workbook excelappworkbook;

        const int vertShift = 5;
        const int defWidth = 12;

        private void setRangeValue(Excel.Worksheet excelworksheet, string rangeFrom, string rangeTo, string Value, Excel.Constants horizAlignment, Excel.Constants vertAlignment, int cellWidth = 0)
        {
            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper());
            excelcells.Value = Value;

            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper(), rangeTo.ToUpper());
            excelcells.Select();
            ((Excel.Range)(excelapp.Selection)).Merge(Type.Missing);
            excelcells.HorizontalAlignment = horizAlignment;
            excelcells.VerticalAlignment = Excel.Constants.xlCenter;

            if (cellWidth > 0) excelcells.EntireColumn.ColumnWidth = cellWidth;
            excelcells.WrapText = true;
        }
        
        public MainForm()
        {
            InitializeComponent();

        }
        // Вводим данные формы
        private void enterDataMenuItem_Click(object sender, EventArgs e)
        {
            InputDataForm inputForm = new InputDataForm(-1);
            inputForm.ShowDialog();

            формаTableAdapter.FillByShort(this.tuberStatisticDBDataSet.Форма);
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            //if (DateTime.Now >= DateTime.Parse("31.12.2013")) Close();

            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet2.ФормаПостоянных". При необходимости она может быть перемещена или удалена.
            this.формаПостоянныхTableAdapter.FillShort(this.tuberStatisticDBDataSet1.ФормаПостоянных);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet1.СтруктурнаяЕдиница". При необходимости она может быть перемещена или удалена.
            this.структурнаяЕдиницаTableAdapter.Fill(this.tuberStatisticDBDataSet1.СтруктурнаяЕдиница);
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.Форма". При необходимости она может быть перемещена или удалена.
            this.формаTableAdapter.FillByShort(this.tuberStatisticDBDataSet.Форма);
            // При загрузке фильтрация выключена
            filterDataOn = false;
            yearStart = yearEnd = quarterStart = quarterEnd = territoryId = null;
            delDataFormFilterButton.Enabled = false;
            // При загрузке фильтрация форм постоянных значений не ведется
            filterConstOn = false;
            yearConstStart = yearConstEnd = quarterConstStart = quarterConstEnd = null;
            delFilterConstButton.Enabled = false;

            
        }
        
        private void exitMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void indicatorMenuItem_Click(object sender, EventArgs e)
        {
            IndicatorsForm iform = new IndicatorsForm();
            iform.Show();
        }

        private void structTypeMenuItem_Click(object sender, EventArgs e)
        {
            StructOneForm sform = new StructOneForm();
            sform.Show();
        }

        private void territoriesMenuItem_Click(object sender, EventArgs e)
        {
            TerritoriesForm tform = new TerritoriesForm();
            tform.Show();
        }

        private void empMenuItem_Click(object sender, EventArgs e)
        {
            EmployeesForm eform = new EmployeesForm();
            eform.Show();
        }


        private void fillByShortToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.формаTableAdapter.FillByShort(this.tuberStatisticDBDataSet.Форма);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        private void fillByToolStripButton_Click(object sender, EventArgs e)
        {
            try
            {
                this.формаTableAdapter.FillBy(this.tuberStatisticDBDataSet.Форма);
            }
            catch (System.Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }

        // Удаляем данные формы
        private void deleteStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить данные формы?", "Внимание!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    int formCode = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                    оперативныйУчетTableAdapter1.DeleteQuery(formCode);
                    формаTableAdapter.DeleteQuery(formCode);
                    формаTableAdapter.FillByShort(tuberStatisticDBDataSet.Форма);
                }
            }
        }
        // Редактируем данные формы
        private void editStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                int formCode = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                InputDataForm inputForm = new InputDataForm(formCode);
                inputForm.ShowDialog();

                формаTableAdapter.FillByShort(this.tuberStatisticDBDataSet.Форма);
            }
        }

        // Экспорт данных формы в Excel
        private void printMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                const int defWidth = 10;
                int vertShift = 6;

                int formCode = (int)dataGridView1.SelectedRows[0].Cells[0].Value;
                DataTable dt = формаTableAdapter.GetDataForPrinting(formCode);

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Данные для построения отчета отсутствуют!");
                    return;
                }
                
                excelapp = new Excel.Application();

                excelapp.Workbooks.Add(Type.Missing);

                excelappworkbook = excelapp.Workbooks[1];

                excelsheets = excelappworkbook.Worksheets;

                //Получаем ссылку на лист 1
                excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

                // Заголовок
                setRangeValue(excelworksheet, "A1", "g1", "Централизованный оперативный учет противотуберкулезной работы", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "a2", "d3","", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                                
                // Территория
                string ter = (string)(dataGridView1.SelectedRows[0].Cells[3].FormattedValue);
                ter = ter.Substring(0,2) == "г." ? ter : ter + " район";

                setRangeValue(excelworksheet, "e2", "g2", ter, Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Дата
                setRangeValue(excelworksheet, "e3", "g3", dataGridView1.SelectedRows[0].Cells[2].Value.ToString() + " квартал " + dataGridView1.SelectedRows[0].Cells[1].Value.ToString(), Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Колонка данных прошлого года этого же периода
                setRangeValue(excelworksheet, "A4", "c4", "За этот же период прошлого года", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "a5", "a5", "взрослые", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "b5", "b5", "подростки", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "c5", "c5", "дети", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                // Наименование показателя
                setRangeValue(excelworksheet, "d4", "d4", "Показатели", Excel.Constants.xlCenter, Excel.Constants.xlCenter,80);

                // Текущие данные
                setRangeValue(excelworksheet, "e4", "g4", "Текущие значения", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "e5", "e5", "взрослые", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "f5", "f5", "подростки", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "g5", "g5", "дети", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    excelcells = excelworksheet.get_Range('A' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу2.ВеличинаВзрослые");

                    excelcells = excelworksheet.get_Range('B' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу2.ВеличинаПодростки");

                    excelcells = excelworksheet.get_Range('C' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу2.ВеличинаДети");

                    excelcells = excelworksheet.get_Range('D' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<string>("Наименование");

                    excelcells = excelworksheet.get_Range('E' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу.ВеличинаВзрослые");

                    excelcells = excelworksheet.get_Range('F' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу.ВеличинаПодростки");

                    excelcells = excelworksheet.get_Range('G' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<int?>("оу.ВеличинаДети");
                }

                vertShift = vertShift + dt.Rows.Count + 1;


                setRangeValue(excelworksheet, "a" + vertShift.ToString(), "a" + vertShift.ToString(), "Фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "b" + vertShift.ToString(), "b" + vertShift.ToString(), "Детские фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                
                // Наименование показателя
                setRangeValue(excelworksheet, "c" + vertShift.ToString(), "e" + vertShift.ToString(), "Штаты фтизиатров", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Текущие данные
                setRangeValue(excelworksheet, "f" + vertShift.ToString(), "f" + vertShift.ToString(), "Фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "g" + vertShift.ToString(), "g" + vertShift.ToString(), "Детские фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                setRangeValue(excelworksheet, "c" + (vertShift + 1).ToString(), "d" + (vertShift + 2).ToString(), "Штатных должностей", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "e" + (vertShift + 1).ToString(), "e" + (vertShift + 1).ToString(), "всего", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                setRangeValue(excelworksheet, "e" + (vertShift + 2).ToString(), "e" + (vertShift + 2).ToString(), "занято", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                
                setRangeValue(excelworksheet, "c" + (vertShift + 3).ToString(), "e" + (vertShift + 3).ToString(), "Физических лиц", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                dt = формаTableAdapter.GetDataByStaffCount(formCode);

                excelcells = excelworksheet.get_Range('A' + (vertShift+1).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ШтатныхДолжностейВсегоФ");
                
                excelcells = excelworksheet.get_Range('A' + (vertShift+2).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ШтатныхДолжностейЗанятоФ");
                
                excelcells = excelworksheet.get_Range('B' + (vertShift+1).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ШтатныхДолжностейВсегоФД");

                excelcells = excelworksheet.get_Range('B' + (vertShift+2).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ШтатныхДолжностейЗанятоФД");

                excelcells = excelworksheet.get_Range('A' + (vertShift+3).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ФизическихЛицФ");
                
                excelcells = excelworksheet.get_Range('B' + (vertShift+3).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф2.ФизическихЛицФД");

                //
                excelcells = excelworksheet.get_Range('F' + (vertShift+1).ToString());
                excelcells.Value = (dt.Rows[0].Field<double?>("ф.ШтатныхДолжностейВсегоФ")).ToString();

                excelcells = excelworksheet.get_Range('F' + (vertShift+2).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф.ШтатныхДолжностейЗанятоФ");

                excelcells = excelworksheet.get_Range('G' + (vertShift+1).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф.ШтатныхДолжностейВсегоФД");

                excelcells = excelworksheet.get_Range('G' + (vertShift+2).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф.ШтатныхДолжностейЗанятоФД");

                excelcells = excelworksheet.get_Range('F' + (vertShift+3).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф.ФизическихЛицФ");

                excelcells = excelworksheet.get_Range('G' + (vertShift+3).ToString());
                excelcells.Value = dt.Rows[0].Field<double?>("ф.ФизическихЛицФД");
                               
                excelcells = excelworksheet.get_Range("A1",'G' + (vertShift + 3).ToString());

                excelcells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;


                setRangeValue(excelworksheet, "A" + (vertShift + 4).ToString(), 'G' + (vertShift + 5).ToString(), "*Примечание: показатели, вошедшие в \"Центральный оперативный учет\""
                    + "взяты из отчетных форм №30 и 33, утвержденных постановлением Госкомстата России №175 от 10.09.2002 г."
                    + "и постановлением Росстата №80 от 11.11.2005 г.", Excel.Constants.xlLeft, Excel.Constants.xlLeft);

                excelcells = excelworksheet.get_Range('E' + (vertShift + 6).ToString());
                excelcells.Value = "Должность";

                excelcells = excelworksheet.get_Range('F' + (vertShift + 6).ToString());
                excelcells.Value = "___________________";

                excelcells = excelworksheet.get_Range('E' + (vertShift + 7).ToString());
                excelcells.Value = "Подпись";

                excelcells = excelworksheet.get_Range('F' + (vertShift + 7).ToString());
                excelcells.Value = "___________________";

                excelapp.Visible = true;

            }
        }

        private void addConstFormMenuItem_Click(object sender, EventArgs e)
        {
            EditConstForm ecform = new EditConstForm();
            ecform.ShowDialog();

            формаПостоянныхTableAdapter.FillShort(tuberStatisticDBDataSet1.ФормаПостоянных);
        }

        private void editConstFormMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView2.SelectedRows != null)
            {
                int formCode = (int)dataGridView2.SelectedRows[0].Cells[0].Value;
                EditConstForm ecform = new EditConstForm(formCode);
                ecform.ShowDialog();

                формаПостоянныхTableAdapter.FillShort(this.tuberStatisticDBDataSet1.ФормаПостоянных);
            }
        }

        private void deleteConstFormMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                DialogResult dialogResult = MessageBox.Show("Вы действительно хотите удалить данные формы?", "Внимание!", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    int formCode = (int)dataGridView2.SelectedRows[0].Cells[0].Value;

                    постоянныеТерриторияTableAdapter1.DeleteFormConsts((int)formCode);
                    формаПостоянныхTableAdapter.DeleteConstForm(formCode);

                    формаПостоянныхTableAdapter.FillShort(this.tuberStatisticDBDataSet1.ФормаПостоянных);
                }
            }
        }

        private void filterConstFormButton_Click(object sender, EventArgs e)
        {
            FilterConstForm fForm = new FilterConstForm();
            if (fForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filterConstOn = true;
                yearConstStart = fForm.yearCheckBox.Checked ? (int?)fForm.yearUpDown1.Value : null;
                yearConstEnd = fForm.yearCheckBox.Checked ? (int?)fForm.yearUpDown2.Value : null;

                quarterConstStart = fForm.quarterCheckBox.Checked ? (int?)fForm.quarterUpDown1.Value : null;
                quarterConstEnd = fForm.quarterCheckBox.Checked ? (int?)fForm.quarterUpDown2.Value : null;

                формаПостоянныхTableAdapter.FillByFilter(tuberStatisticDBDataSet1.ФормаПостоянных, yearConstStart, yearConstEnd, quarterConstStart, quarterConstEnd);
                delFilterConstButton.Enabled = true;
            }
        }

        private void filterDataFormButton_Click(object sender, EventArgs e)
        {
            FilterDataForm fForm = new FilterDataForm();
            if (fForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                filterDataOn = true;
                yearStart = fForm.yearCheckBox.Checked ? (int?)fForm.yearUpDown1.Value : null;
                yearEnd = fForm.yearCheckBox.Checked ? (int?)fForm.yearUpDown2.Value : null;

                quarterStart = fForm.quarterCheckBox.Checked ? (int?)fForm.quarterUpDown1.Value : null;
                quarterEnd = fForm.quarterCheckBox.Checked ? (int?)fForm.quarterUpDown2.Value : null;

                territoryId = fForm.territoryCheckBox.Checked ? (int?)fForm.territoryComboBox.SelectedValue : null;

                this.формаTableAdapter.FillByFilter(tuberStatisticDBDataSet.Форма, yearStart, yearEnd, quarterStart, quarterEnd, territoryId);
                delDataFormFilterButton.Enabled = true;
            }
        }

        private void delDataFormFilterButton_Click(object sender, EventArgs e)
        {
            filterDataOn = false;
            yearStart = yearEnd = quarterStart = quarterEnd = territoryId = null;
            this.формаTableAdapter.FillByShort(tuberStatisticDBDataSet.Форма);
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            if (filterDataOn)
                this.формаTableAdapter.FillByFilter(tuberStatisticDBDataSet.Форма, yearStart, yearEnd, quarterStart, quarterEnd, territoryId);
            else 
                this.формаTableAdapter.FillByShort(tuberStatisticDBDataSet.Форма);
        }

        private void tuberReportMenuItem_Click(object sender, EventArgs e)
        {
            TubDiagReportSettingsForm tform = new TubDiagReportSettingsForm();
            if (tform.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            { 
                // Строим отчет




            
            }
        }

        private void delFilterConstButton_Click(object sender, EventArgs e)
        {
            filterConstOn = false;
            yearConstStart = yearConstEnd = quarterConstStart = quarterConstEnd = null;
            this.формаПостоянныхTableAdapter.FillShort(tuberStatisticDBDataSet1.ФормаПостоянных);
        }

        private void refreshConstDataButton_Click(object sender, EventArgs e)
        {
            if (filterConstOn)
                формаПостоянныхTableAdapter.FillByFilter(tuberStatisticDBDataSet1.ФормаПостоянных, yearConstStart, yearConstEnd, quarterConstStart, quarterConstEnd);
            else
                this.формаПостоянныхTableAdapter.FillShort(tuberStatisticDBDataSet1.ФормаПостоянных);

        }

        private void mainMenu_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void commonReportMenuItem_Click(object sender, EventArgs e)
        {
            CommonReportForm cf = new CommonReportForm();
            cf.ShowDialog();
        }

        private void printConstFormMenuItem_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows != null)
            {
                const int defWidth = 10;
                int vertShift = 4;

                int formCode = (int)dataGridView2.SelectedRows[0].Cells[0].Value;
                DataTable dt = формаПостоянныхTableAdapter.GetDataByConstFormPrintData(formCode);

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Данные для построения отчета отсутствуют!");
                    return;
                }

                excelapp = new Excel.Application();

                excelapp.Workbooks.Add(Type.Missing);

                excelappworkbook = excelapp.Workbooks[1];

                excelsheets = excelappworkbook.Worksheets;

                //Получаем ссылку на лист 1
                excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

                // Заголовок
                setRangeValue(excelworksheet, "A1", "i1", "Лист постоянных ЦОК за " + dataGridView2.SelectedRows[0].Cells[2].Value.ToString() + " квартал " + dataGridView2.SelectedRows[0].Cells[1].Value.ToString()
                    + " г.", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "a2", "a3", "Территория", Excel.Constants.xlCenter, Excel.Constants.xlCenter,30);
                
                // Население
                setRangeValue(excelworksheet, "b2", "e2", "Население", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                setRangeValue(excelworksheet, "b3", "b3", "всего", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "c3", "c3", "взрослое", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "d3", "d3", "подрост.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "e3", "e3", "детское", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Постоянные
                setRangeValue(excelworksheet, "f2", "i2", "Постоянные", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                
                setRangeValue(excelworksheet, "f3", "f3", "Состоит на учете бациллярных больных", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                setRangeValue(excelworksheet, "g3", "g3", "Число бациллярных больных среди в/в прошлого года", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "h3", "h3", "Контингент активных больных туб. органов дыхания", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                setRangeValue(excelworksheet, "i3", "i3", "Контингент активных больных внелегочным туберкулезом", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    excelcells = excelworksheet.get_Range('A' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<string>("Наименование");

                    excelcells = excelworksheet.get_Range('B' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("Всего");

                    excelcells = excelworksheet.get_Range('C' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("НаселениеВзрослое");

                    excelcells = excelworksheet.get_Range('D' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("НаселениеПодростки");

                    excelcells = excelworksheet.get_Range('E' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("НаселениеДети");

                    excelcells = excelworksheet.get_Range('F' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("БациллярныеБольные");

                    excelcells = excelworksheet.get_Range('G' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("БацилБольныеВВ_ПрошлыйГод");

                    excelcells = excelworksheet.get_Range('H' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("БольныеТуберОргДыхания");

                    excelcells = excelworksheet.get_Range('I' + (i + vertShift).ToString());
                    excelcells.Value = dt.Rows[i].Field<double?>("БольныеТуберВнелегочный");
                }

                vertShift = vertShift + dt.Rows.Count-1;
                excelcells = excelworksheet.get_Range('A' + (vertShift).ToString());

                if (excelcells.Value != "Тот же период прошлого года")
                {
                    vertShift++;
                    excelcells = excelworksheet.get_Range('A' + (vertShift).ToString());
                    excelcells.Value = "Тот же период прошлого года";
                    setRangeValue(excelworksheet, "b" + (vertShift).ToString(), "i" + (vertShift).ToString(), "Данные в БД отсутствуют", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                }

                excelcells = excelworksheet.get_Range("A1", 'I' + (vertShift).ToString());

                excelcells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                excelapp.Visible = true;
            }
        }

        private void aboutFormMenuItem_Click(object sender, EventArgs e)
        {
            AboutForm af = new AboutForm();
            af.ShowDialog();
        }

        private void commonInputReportMenuItem_Click(object sender, EventArgs e)
        {
            CommonInputDataReport cf = new CommonInputDataReport();
            cf.ShowDialog();
        }

    }
}
