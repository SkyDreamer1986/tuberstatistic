﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TuberStatistic
{
    using Excel = Microsoft.Office.Interop.Excel;

    public partial class TubDiagReportSettingsForm : Form
    {
        public TubDiagReportSettingsForm()
        {
            InitializeComponent();
        }

        Excel.Application excelapp;
        Excel.Sheets excelsheets;
        Excel.Worksheet excelworksheet;
        Excel.Range excelcells;
        Excel.Workbook excelappworkbook;

        const int vertShift = 4;
        const int defWidth = 12;

        private void setRangeValue(Excel.Worksheet excelworksheet, string rangeFrom, string rangeTo, string Value, Excel.Constants horizAlignment, Excel.Constants vertAlignment, int cellWidth = 0)
        { 
        
                excelcells = excelworksheet.get_Range(rangeFrom);
                excelcells.Value = Value;

                excelcells=excelworksheet.get_Range(rangeFrom,rangeTo);
                excelcells.Select();
                ((Excel.Range)(excelapp.Selection)).Merge(Type.Missing);
                excelcells.HorizontalAlignment = Excel.Constants.xlCenter;
                excelcells.VerticalAlignment = Excel.Constants.xlCenter;

                if (cellWidth > 0) excelcells.EntireColumn.ColumnWidth = cellWidth;
                excelcells.WrapText = true;
        }


        private void okButton_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = формаTableAdapter1.GetDataByTuberculDiag((int)yearUpDown1.Value, (int)quarterUpDown1.Value);

                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Данные для построения отчета отсутствуют!");
                    return;
                }

                excelapp = new Excel.Application();

                excelapp.Workbooks.Add(Type.Missing);

                excelappworkbook =  excelapp.Workbooks[1];

                excelsheets = excelappworkbook.Worksheets;
                //Получаем ссылку на лист 1
                excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

                // Заголовок
                setRangeValue(excelworksheet, "A1", "P1", "Отчет за " + ((int)quarterUpDown1.Value).ToString() + " квартал " + ((int)yearUpDown1.Value).ToString(), Excel.Constants.xlCenter, Excel.Constants.xlCenter, 35);

                // Колонка территорий
                setRangeValue(excelworksheet, "A2", "A4", "Наименование городов и районов", Excel.Constants.xlCenter, Excel.Constants.xlCenter, 35);
                
                // Тубдиагностика
                setRangeValue(excelworksheet, "B2", "K2", "Тубдиагностика", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                
                // Подростки
                setRangeValue(excelworksheet, "B3", "F3", "Подростки", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Подлежит
                setRangeValue(excelworksheet, "B4", "B4", "Подлежит", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Сделано
                setRangeValue(excelworksheet, "C4", "C4", "Сделано", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "D4", "D4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Виражей
                setRangeValue(excelworksheet, "E4", "E4", "Виражей", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Гиперпроб
                setRangeValue(excelworksheet, "F4", "F4", "Гиперпроб", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                
                // Дети
                setRangeValue(excelworksheet, "G3", "K3", "Дети", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Подлежит
                setRangeValue(excelworksheet, "G4", "G4", "Подлежит", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Сделано
                setRangeValue(excelworksheet, "H4", "H4", "Сделано", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "I4", "I4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Виражей
                setRangeValue(excelworksheet, "J4", "J4", "Виражей", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Гиперпроб
                setRangeValue(excelworksheet, "K4", "K4", "Гиперпроб", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);


                // Ревакцинация
                setRangeValue(excelworksheet, "L2", "P2", "Ревакцинация", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Ревакцинация плановая
                setRangeValue(excelworksheet, "L3", "M3", "План", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Дети 7 лет
                setRangeValue(excelworksheet, "L4", "L4", "Дети 7 лет", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Дети 14 лет
                setRangeValue(excelworksheet, "M4", "M4", "Дети 14 лет", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);


                // Ревакцинация проведенная
                setRangeValue(excelworksheet, "N3", "O3", "Сделано", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Дети 7 лет
                setRangeValue(excelworksheet, "N4", "N4", "Дети 7 лет", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Дети 14 лет
                setRangeValue(excelworksheet, "O4", "O4", "Дети 14 лет", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                               
                // Ревакцинация проведенная
                setRangeValue(excelworksheet, "P3", "P3", "", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                
                // Ревакцинация проведенная
                setRangeValue(excelworksheet, "P4", "P4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    // Территория
                    excelcells = excelworksheet.get_Range("A" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<string>("Наименование");

                    // Подростки: Подлежит
                    excelcells = excelworksheet.get_Range("B" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ПодросткиПодлежит");

                    // Подростки: Сделано
                    excelcells = excelworksheet.get_Range("C" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ПодросткиСделано");
                    
                    // Подростки: Выполнено
                    excelcells = excelworksheet.get_Range("D" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ПодросткиВыполнено");
                    excelcells.NumberFormat = "0.00";

                    // Подростки: Виражей
                    excelcells = excelworksheet.get_Range("E" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ПодросткиВиражей");

                    // Подростки: Гиперпроб
                    excelcells = excelworksheet.get_Range("F" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ПодросткиГиперпроб");


                    // Дети: Подлежит
                    excelcells = excelworksheet.get_Range("G" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ДетиПодлежит");

                    // Дети: Сделано
                    excelcells = excelworksheet.get_Range("H" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ДетиСделано");

                    // Дети: Выполнено
                    excelcells = excelworksheet.get_Range("I" + (i + vertShift + 1).ToString());
                    excelcells.NumberFormat = "0.00";
                    excelcells.Value = dt.Rows[i].Field<double>("ДетиВыполнено");
                    
                    // Дети: Виражей
                    excelcells = excelworksheet.get_Range("J" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ДетиВиражей");

                    // Дети: Гиперпроб
                    excelcells = excelworksheet.get_Range("K" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("ДетиГиперпроб");


                    // План. Дети 7 лет 
                    excelcells = excelworksheet.get_Range("L" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("РевПлан7");

                    // План. Дети 14 лет
                    excelcells = excelworksheet.get_Range("M" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("РевПлан14");

                    // Сделано. Дети 7 лет
                    excelcells = excelworksheet.get_Range("N" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("РевСделано7");

                    // Сделано. Дети 14 лет
                    excelcells = excelworksheet.get_Range("O" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("РевСделано14");

                    // Выполнено
                    excelcells = excelworksheet.get_Range("P" + (i + vertShift + 1).ToString());
                    excelcells.Value = dt.Rows[i].Field<double>("РевакцинацияВыполнено");
                    excelcells.EntireColumn.ColumnWidth = defWidth;
                    excelcells.NumberFormat = "0.00";
                }

                excelcells = excelworksheet.get_Range("A1", 'P' + (dt.Rows.Count + vertShift).ToString());

                excelcells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                excelapp.Visible = true;
            }
            catch
            {
                
            }
            
        }

        private void TubDiagReportSettingsForm_Load(object sender, EventArgs e)
        {
            yearUpDown1.Minimum = (int)this.формаTableAdapter1.GetFirstYear();
            yearUpDown1.Value = yearUpDown1.Maximum = (int)this.формаTableAdapter1.GetLastYear();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
