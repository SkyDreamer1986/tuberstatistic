﻿namespace TuberStatistic
{
    partial class FilterConstForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.quarterCheckBox = new System.Windows.Forms.CheckBox();
            this.quarterUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.quarterUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.yearCheckBox = new System.Windows.Forms.CheckBox();
            this.yearUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.yearUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.формаПостоянныхTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown1)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.quarterCheckBox);
            this.groupBox2.Controls.Add(this.quarterUpDown2);
            this.groupBox2.Controls.Add(this.quarterUpDown1);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(2, 63);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(264, 60);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "    Квартал";
            // 
            // quarterCheckBox
            // 
            this.quarterCheckBox.AutoSize = true;
            this.quarterCheckBox.Location = new System.Drawing.Point(6, 0);
            this.quarterCheckBox.Name = "quarterCheckBox";
            this.quarterCheckBox.Size = new System.Drawing.Size(15, 14);
            this.quarterCheckBox.TabIndex = 25;
            this.quarterCheckBox.UseVisualStyleBackColor = true;
            this.quarterCheckBox.CheckedChanged += new System.EventHandler(this.quarterCheckBox_CheckedChanged);
            // 
            // quarterUpDown2
            // 
            this.quarterUpDown2.Location = new System.Drawing.Point(163, 25);
            this.quarterUpDown2.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.quarterUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quarterUpDown2.Name = "quarterUpDown2";
            this.quarterUpDown2.Size = new System.Drawing.Size(94, 20);
            this.quarterUpDown2.TabIndex = 24;
            this.quarterUpDown2.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // quarterUpDown1
            // 
            this.quarterUpDown1.Location = new System.Drawing.Point(32, 25);
            this.quarterUpDown1.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.quarterUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quarterUpDown1.Name = "quarterUpDown1";
            this.quarterUpDown1.Size = new System.Drawing.Size(92, 20);
            this.quarterUpDown1.TabIndex = 23;
            this.quarterUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(14, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "С";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(138, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(19, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "по";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.yearCheckBox);
            this.groupBox1.Controls.Add(this.yearUpDown2);
            this.groupBox1.Controls.Add(this.yearUpDown1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(264, 60);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "    Год";
            // 
            // yearCheckBox
            // 
            this.yearCheckBox.AutoSize = true;
            this.yearCheckBox.Location = new System.Drawing.Point(6, 0);
            this.yearCheckBox.Name = "yearCheckBox";
            this.yearCheckBox.Size = new System.Drawing.Size(15, 14);
            this.yearCheckBox.TabIndex = 26;
            this.yearCheckBox.UseVisualStyleBackColor = true;
            // 
            // yearUpDown2
            // 
            this.yearUpDown2.Location = new System.Drawing.Point(163, 25);
            this.yearUpDown2.Name = "yearUpDown2";
            this.yearUpDown2.Size = new System.Drawing.Size(94, 20);
            this.yearUpDown2.TabIndex = 24;
            // 
            // yearUpDown1
            // 
            this.yearUpDown1.Location = new System.Drawing.Point(32, 25);
            this.yearUpDown1.Name = "yearUpDown1";
            this.yearUpDown1.Size = new System.Drawing.Size(92, 20);
            this.yearUpDown1.TabIndex = 23;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(14, 13);
            this.label3.TabIndex = 22;
            this.label3.Text = "С";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(138, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(19, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "по";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(2, 123);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(264, 49);
            this.panel1.TabIndex = 25;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(95, 16);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.ForeColor = System.Drawing.Color.Black;
            this.okButton.Location = new System.Drawing.Point(176, 16);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // формаПостоянныхTableAdapter1
            // 
            this.формаПостоянныхTableAdapter1.ClearBeforeFill = true;
            // 
            // FilterConstForm
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cancelButton;
            this.ClientSize = new System.Drawing.Size(271, 174);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FilterConstForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Фильтрация";
            this.Load += new System.EventHandler(this.FilterConstForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.CheckBox quarterCheckBox;
        public System.Windows.Forms.NumericUpDown quarterUpDown2;
        public System.Windows.Forms.NumericUpDown quarterUpDown1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.CheckBox yearCheckBox;
        public System.Windows.Forms.NumericUpDown yearUpDown2;
        public System.Windows.Forms.NumericUpDown yearUpDown1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter формаПостоянныхTableAdapter1;
    }
}