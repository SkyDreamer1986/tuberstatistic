﻿namespace TuberStatistic
{
    partial class InputDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.quarterUpDown = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.yearUpDown = new System.Windows.Forms.NumericUpDown();
            this.okButton = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.territoryComboBox = new System.Windows.Forms.ComboBox();
            this.структурнаяЕдиницаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tuberStatisticDBDataSet = new TuberStatistic.TuberStatisticDBDataSet();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbFLF = new System.Windows.Forms.TextBox();
            this.tbStFE = new System.Windows.Forms.TextBox();
            this.tbStFAll = new System.Windows.Forms.TextBox();
            this.tbFLCF = new System.Windows.Forms.TextBox();
            this.tbStCFE = new System.Windows.Forms.TextBox();
            this.tbStCFAll = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.кодDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодФормаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодПоказательDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.показательBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.величинаВзрослыеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.величинаПодросткиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.величинаДетиDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.оперативныйУчетBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.структурнаяЕдиницаTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter();
            this.оперативныйУчетTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ОперативныйУчетTableAdapter();
            this.показательTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ПоказательTableAdapter();
            this.формаTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.показательBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.оперативныйУчетBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(738, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Год";
            // 
            // quarterUpDown
            // 
            this.quarterUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.quarterUpDown.Location = new System.Drawing.Point(664, 16);
            this.quarterUpDown.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.quarterUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.quarterUpDown.Name = "quarterUpDown";
            this.quarterUpDown.Size = new System.Drawing.Size(68, 20);
            this.quarterUpDown.TabIndex = 6;
            this.quarterUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(609, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Квартал";
            // 
            // yearUpDown
            // 
            this.yearUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.yearUpDown.Location = new System.Drawing.Point(776, 16);
            this.yearUpDown.Maximum = new decimal(new int[] {
            2100,
            0,
            0,
            0});
            this.yearUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.yearUpDown.Name = "yearUpDown";
            this.yearUpDown.Size = new System.Drawing.Size(68, 20);
            this.yearUpDown.TabIndex = 8;
            this.yearUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.ForeColor = System.Drawing.Color.Black;
            this.okButton.Location = new System.Drawing.Point(769, 16);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(0, 526);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(857, 49);
            this.panel1.TabIndex = 10;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(688, 16);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // territoryComboBox
            // 
            this.territoryComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.territoryComboBox.DataSource = this.структурнаяЕдиницаBindingSource;
            this.territoryComboBox.DisplayMember = "Наименование";
            this.territoryComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.territoryComboBox.FormattingEnabled = true;
            this.territoryComboBox.Location = new System.Drawing.Point(368, 16);
            this.territoryComboBox.Name = "territoryComboBox";
            this.territoryComboBox.Size = new System.Drawing.Size(235, 21);
            this.territoryComboBox.TabIndex = 11;
            this.territoryComboBox.ValueMember = "Код";
            // 
            // структурнаяЕдиницаBindingSource
            // 
            this.структурнаяЕдиницаBindingSource.DataMember = "СтруктурнаяЕдиница";
            this.структурнаяЕдиницаBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // tuberStatisticDBDataSet
            // 
            this.tuberStatisticDBDataSet.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(295, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Территория";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tbFLF);
            this.groupBox1.Controls.Add(this.tbStFE);
            this.groupBox1.Controls.Add(this.tbStFAll);
            this.groupBox1.Controls.Add(this.tbFLCF);
            this.groupBox1.Controls.Add(this.tbStCFE);
            this.groupBox1.Controls.Add(this.tbStCFAll);
            this.groupBox1.Location = new System.Drawing.Point(0, 402);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(857, 118);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Штаты фтизиатров";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(435, 90);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "Физических лиц";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(379, 64);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(153, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Штатных должностей занято";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(379, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(147, 13);
            this.label6.TabIndex = 8;
            this.label6.Text = "Штатных должностей всего";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(697, 16);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Детские фтизиатры";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(541, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Фтизиатры";
            // 
            // tbFLF
            // 
            this.tbFLF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFLF.Location = new System.Drawing.Point(532, 87);
            this.tbFLF.Name = "tbFLF";
            this.tbFLF.Size = new System.Drawing.Size(149, 20);
            this.tbFLF.TabIndex = 5;
            // 
            // tbStFE
            // 
            this.tbStFE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStFE.Location = new System.Drawing.Point(532, 61);
            this.tbStFE.Name = "tbStFE";
            this.tbStFE.Size = new System.Drawing.Size(149, 20);
            this.tbStFE.TabIndex = 4;
            // 
            // tbStFAll
            // 
            this.tbStFAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStFAll.Location = new System.Drawing.Point(532, 35);
            this.tbStFAll.Name = "tbStFAll";
            this.tbStFAll.Size = new System.Drawing.Size(149, 20);
            this.tbStFAll.TabIndex = 3;
            // 
            // tbFLCF
            // 
            this.tbFLCF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbFLCF.Location = new System.Drawing.Point(688, 87);
            this.tbFLCF.Name = "tbFLCF";
            this.tbFLCF.Size = new System.Drawing.Size(149, 20);
            this.tbFLCF.TabIndex = 2;
            // 
            // tbStCFE
            // 
            this.tbStCFE.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStCFE.Location = new System.Drawing.Point(688, 61);
            this.tbStCFE.Name = "tbStCFE";
            this.tbStCFE.Size = new System.Drawing.Size(149, 20);
            this.tbStCFE.TabIndex = 1;
            // 
            // tbStCFAll
            // 
            this.tbStCFAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbStCFAll.Location = new System.Drawing.Point(688, 35);
            this.tbStCFAll.Name = "tbStCFAll";
            this.tbStCFAll.Size = new System.Drawing.Size(149, 20);
            this.tbStCFAll.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(0, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(857, 352);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Показатели";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодDataGridViewTextBoxColumn,
            this.кодФормаDataGridViewTextBoxColumn,
            this.кодПоказательDataGridViewTextBoxColumn,
            this.величинаВзрослыеDataGridViewTextBoxColumn,
            this.величинаПодросткиDataGridViewTextBoxColumn,
            this.величинаДетиDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.оперативныйУчетBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(6, 19);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(840, 327);
            this.dataGridView1.TabIndex = 3;
            // 
            // кодDataGridViewTextBoxColumn
            // 
            this.кодDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодDataGridViewTextBoxColumn.DataPropertyName = "Код";
            this.кодDataGridViewTextBoxColumn.HeaderText = "Код";
            this.кодDataGridViewTextBoxColumn.Name = "кодDataGridViewTextBoxColumn";
            this.кодDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // кодФормаDataGridViewTextBoxColumn
            // 
            this.кодФормаDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодФормаDataGridViewTextBoxColumn.DataPropertyName = "Код_Форма";
            this.кодФормаDataGridViewTextBoxColumn.HeaderText = "Код_Форма";
            this.кодФормаDataGridViewTextBoxColumn.Name = "кодФормаDataGridViewTextBoxColumn";
            this.кодФормаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодФормаDataGridViewTextBoxColumn.Visible = false;
            // 
            // кодПоказательDataGridViewTextBoxColumn
            // 
            this.кодПоказательDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.кодПоказательDataGridViewTextBoxColumn.DataPropertyName = "Код_Показатель";
            this.кодПоказательDataGridViewTextBoxColumn.DataSource = this.показательBindingSource;
            this.кодПоказательDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодПоказательDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодПоказательDataGridViewTextBoxColumn.HeaderText = "Показатель";
            this.кодПоказательDataGridViewTextBoxColumn.Name = "кодПоказательDataGridViewTextBoxColumn";
            this.кодПоказательDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодПоказательDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодПоказательDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.кодПоказательDataGridViewTextBoxColumn.ValueMember = "Код";
            // 
            // показательBindingSource
            // 
            this.показательBindingSource.DataMember = "Показатель";
            this.показательBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // величинаВзрослыеDataGridViewTextBoxColumn
            // 
            this.величинаВзрослыеDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.величинаВзрослыеDataGridViewTextBoxColumn.DataPropertyName = "ВеличинаВзрослые";
            this.величинаВзрослыеDataGridViewTextBoxColumn.HeaderText = "Взрослые";
            this.величинаВзрослыеDataGridViewTextBoxColumn.Name = "величинаВзрослыеDataGridViewTextBoxColumn";
            // 
            // величинаПодросткиDataGridViewTextBoxColumn
            // 
            this.величинаПодросткиDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.величинаПодросткиDataGridViewTextBoxColumn.DataPropertyName = "ВеличинаПодростки";
            this.величинаПодросткиDataGridViewTextBoxColumn.HeaderText = "Подростки";
            this.величинаПодросткиDataGridViewTextBoxColumn.Name = "величинаПодросткиDataGridViewTextBoxColumn";
            // 
            // величинаДетиDataGridViewTextBoxColumn
            // 
            this.величинаДетиDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.величинаДетиDataGridViewTextBoxColumn.DataPropertyName = "ВеличинаДети";
            this.величинаДетиDataGridViewTextBoxColumn.HeaderText = "Дети";
            this.величинаДетиDataGridViewTextBoxColumn.Name = "величинаДетиDataGridViewTextBoxColumn";
            // 
            // оперативныйУчетBindingSource
            // 
            this.оперативныйУчетBindingSource.DataMember = "ОперативныйУчет";
            this.оперативныйУчетBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // структурнаяЕдиницаTableAdapter
            // 
            this.структурнаяЕдиницаTableAdapter.ClearBeforeFill = true;
            // 
            // оперативныйУчетTableAdapter
            // 
            this.оперативныйУчетTableAdapter.ClearBeforeFill = true;
            // 
            // показательTableAdapter
            // 
            this.показательTableAdapter.ClearBeforeFill = true;
            // 
            // формаTableAdapter1
            // 
            this.формаTableAdapter1.ClearBeforeFill = true;
            // 
            // InputDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 577);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.territoryComboBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.yearUpDown);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.quarterUpDown);
            this.Controls.Add(this.label1);
            this.Name = "InputDataForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Централизованный оперативный учет противотуберкулезной работы на территории";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.InputDataForm_FormClosing);
            this.Load += new System.EventHandler(this.InputDataForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.quarterUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.yearUpDown)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.показательBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.оперативныйУчетBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown quarterUpDown;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown yearUpDown;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.ComboBox territoryComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbFLF;
        private System.Windows.Forms.TextBox tbStFE;
        private System.Windows.Forms.TextBox tbStFAll;
        private System.Windows.Forms.TextBox tbFLCF;
        private System.Windows.Forms.TextBox tbStCFE;
        private System.Windows.Forms.TextBox tbStCFAll;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet;
        private System.Windows.Forms.BindingSource структурнаяЕдиницаBindingSource;
        private TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter структурнаяЕдиницаTableAdapter;
        private System.Windows.Forms.BindingSource оперативныйУчетBindingSource;
        private TuberStatisticDBDataSetTableAdapters.ОперативныйУчетTableAdapter оперативныйУчетTableAdapter;
        private System.Windows.Forms.BindingSource показательBindingSource;
        private TuberStatisticDBDataSetTableAdapters.ПоказательTableAdapter показательTableAdapter;
        private TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter формаTableAdapter1;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодФормаDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодПоказательDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn величинаВзрослыеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn величинаПодросткиDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn величинаДетиDataGridViewTextBoxColumn;
    }
}