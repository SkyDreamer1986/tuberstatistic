﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class StructOneForm : Form
    {
        public StructOneForm()
        {
            InitializeComponent();
        }

        private void StructOneForm_Load(object sender, EventArgs e)
        {
            // TODO: данная строка кода позволяет загрузить данные в таблицу "tuberStatisticDBDataSet.ТипСтруктурнойЕдиницы". При необходимости она может быть перемещена или удалена.
            this.типСтруктурнойЕдиницыTableAdapter.Fill(this.tuberStatisticDBDataSet.ТипСтруктурнойЕдиницы);

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            this.типСтруктурнойЕдиницыTableAdapter.Update(this.tuberStatisticDBDataSet.ТипСтруктурнойЕдиницы);
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Сохранить изменения?", "Внимание!", MessageBoxButtons.YesNo);

            if (dialogResult == System.Windows.Forms.DialogResult.Yes)
                this.okButton_Click(sender, e);
            else
                Close();
        }
    }
}
