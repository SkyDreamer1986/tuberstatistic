﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Linq;

namespace TuberStatistic
{
    public partial class CommonReportForm : Form
    {
        public CommonReportForm()
        {
            InitializeComponent();
        }

        Excel.Application excelapp;
        Excel.Sheets excelsheets;
        Excel.Worksheet excelworksheet;
        Excel.Range excelcells;
        Excel.Workbook excelappworkbook;

        static int[] p = {2, 4, 6, 8, 10, 12, 14, 16, 18, 21, 23, 25, 27, 30, 33, 35, 37, 40, 43, 46, 48, 50};

        const int vertShift = 5;
        const int defWidth = 12;

        private void setRangeValue(Excel.Worksheet excelworksheet, string rangeFrom, string rangeTo, string Value, Excel.Constants horizAlignment, Excel.Constants vertAlignment, int cellWidth = 0)
        {
            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper());
            excelcells.Value = Value;

            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper(), rangeTo.ToUpper());
            excelcells.Select();
            ((Excel.Range)(excelapp.Selection)).Merge(Type.Missing);
            excelcells.HorizontalAlignment = Excel.Constants.xlCenter;
            excelcells.VerticalAlignment = Excel.Constants.xlCenter;

            if (cellWidth > 0) excelcells.EntireColumn.ColumnWidth = cellWidth;
            excelcells.WrapText = true;
        }
        
        private void CommonReportForm_Load(object sender, EventArgs e)
        {
            yearUpDown1.Minimum = (int)this.формаTableAdapter1.GetFirstYear();
            yearUpDown1.Value = yearUpDown1.Maximum = (int)this.формаTableAdapter1.GetLastYear();

            empСheckBox.Checked = true;
            empСheckBox.Checked = false;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
           // try
            //{
                TuberStatisticDBDataSetTableAdapters.СводныйОтчетПТРTableAdapter ta = new TuberStatisticDBDataSetTableAdapters.СводныйОтчетПТРTableAdapter();

                DataTable dt =  !empСheckBox.Checked  ? this.формаTableAdapter1.GetDataByCommonReportData((int)yearUpDown1.Value, (int)quarterUpDown1.Value)
                    : this.формаTableAdapter1.GetDataByCommonReportEmp((int)yearUpDown1.Value, (int)quarterUpDown1.Value, (int)empComboBox.SelectedValue);
                
                if (dt.Rows.Count == 0)
                {
                    MessageBox.Show("Данные для построения отчета отсутствуют!");
                    return;
                }

                excelapp = new Excel.Application();

                excelapp.Workbooks.Add(Type.Missing);

                excelappworkbook = excelapp.Workbooks[1];

                excelsheets = excelappworkbook.Worksheets;
                
                //Получаем ссылку на лист 1
                excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

                // Заголовок
                setRangeValue(excelworksheet, "A1", "AY1", "Отчет за " + ((int)quarterUpDown1.Value).ToString() + " квартал " + ((int)yearUpDown1.Value).ToString(), Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Колонка территорий
                setRangeValue(excelworksheet, "A2", "A4", "Территория", Excel.Constants.xlCenter, Excel.Constants.xlCenter, 30);

                // Заболеваемость туберкулезом (в пересчете на год)
                setRangeValue(excelworksheet, "B2", "g2", "Заболеваемость туберкулезом (в пересчете на год)", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // всего населения
                setRangeValue(excelworksheet, "B3", "c3", "всего населения", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "B4", "B4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "c4", "c4", "на 100 тыс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // подростков
                setRangeValue(excelworksheet, "d3", "e3", "подростков", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "d4", "d4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "e4", "e4", "на 100 тыс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // детей
                setRangeValue(excelworksheet, "f3", "g3", "детей", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "f4", "f4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "g4", "g4", "на 100 тыс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                
                
                // В/в с туберкулезом органов дыхания
                setRangeValue(excelworksheet, "h2", "k2", "В/в с туберкулезом органов дыхания", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                        
                // Фаза распада
                setRangeValue(excelworksheet, "h3", "i3", "Фаза распада", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "h4", "h4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // %
                setRangeValue(excelworksheet, "i4", "i4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter);


                // БК(+)
                setRangeValue(excelworksheet, "j3", "k3", "БК(+)", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "j4", "j4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // %
                setRangeValue(excelworksheet, "k4", "k4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // Выявлено при профосмотре
                setRangeValue(excelworksheet, " l2", "m3", "Выявлено при профосмотре", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, " l4", "l4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, " m4", "m4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Запущенные в/в (с ФКТ и ДТЛ с распадом)
                setRangeValue(excelworksheet, " n2", "o3", "Запущенные в/в", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, " n4", "n4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, " o4", "o4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Частота рецидивов из III гр. ДУ (в пересчете на год)
                setRangeValue(excelworksheet, " p2", "q3", "Частота рецидивов из III гр. ДУ (в пересчете на год)", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "p4", "p4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "q4", "q4", "на 100 тыс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Госпитализация в/в
                setRangeValue(excelworksheet, "r2", "s3", "Госпитализация в/в", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "r4", "r4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "s4", "s4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);



                // Обследование \"оторвавшихся\" от флюоросмотров
                setRangeValue(excelworksheet, "t2", "u2", "Количество", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
                
                // абс.
                setRangeValue(excelworksheet, "t3", "t3", '"' + "оторвавшихся" + '"' + " от флюороосмотров", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "t4", "t4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "u3", "u3", "привлеченных", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                setRangeValue(excelworksheet, "u4", "u4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "v2", "v3", "привлеченных к  флюорографии", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                setRangeValue(excelworksheet, "v4", "v4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Смертность от активного туберкулеза в пересчете на год
                setRangeValue(excelworksheet, "w2", "x3", "Смертность от активного туберкулеза в пересчете на год", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "w4", "w4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "x4", "x4", "на 100 т. нас.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Умерло до 1 года учета
                setRangeValue(excelworksheet, "y2", "z3", "Умерло до 1 года учета", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "y4", "y4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "z4", "z4", "на 100 т. нас.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Умерло неизвестных
                setRangeValue(excelworksheet, "aa2", "ab3", "Умерло неизвестных", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "aa4", "aa4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ab4", "ab4", "на 100 т. нас.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Госпитализация больных II группы ДУ
                setRangeValue(excelworksheet, "ac2", "ae2", "Госпитализация больных II группы ДУ", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "ac3", "ac4", "госпитализировано", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ad3", "ad4", "подлежало", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ae3", "ae4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Закрытие каверн у в/в прошлого года
                setRangeValue(excelworksheet, "af2", "ah2", "Закрытие каверн у в/в прошлого года", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "af3", "af4", "закрылось", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ag3", "ag4", "состоит", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ah3", "ah4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Распространенность активных форм туберкулеза
                setRangeValue(excelworksheet, "ai2", "aj3", "Болезненность активными формами туберкулеза", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "ai4", "ai4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "aj4", "aj4", "на 100 т. нас.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Распространенность деструктивных форм туберкулеза органов дыхания
                setRangeValue(excelworksheet, "ak2", "al3", "Болезненность деструктивными формами туберкулеза органов дыхания", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "ak4", "ak4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "al4", "al4", "на 100 т. нас.", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Абациллировано из контингентов
                setRangeValue(excelworksheet, "am2", "ao2", "Абациллировано из контингентов", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "am3", "am4", "количество абациллированных", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "an3", "an4", "состоит на учете", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ao3", "ao4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // Прекращение бактериовыделения у в/в
                setRangeValue(excelworksheet, "ap2", "ar2", "Прекращение бактериовыделения у в/в", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "ap3", "ap4", "прекр. бакт. у в/в", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "aq3", "aq4", "Бацил. больные среди в/в прошлого года", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "ar3", "ar4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);


                // Клиническое излечение
                setRangeValue(excelworksheet, "as2", "au2", "Клиническое излечение", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // абс.
                setRangeValue(excelworksheet, "as3", "as4", "переведено в III гр. ДУ (абс.)", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "at3", "at4", "переведено в I-II гр. ДУ (абс.)", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // %
                setRangeValue(excelworksheet, "au3", "au4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
                        
                // Прооперировано больных туберкулезом
                setRangeValue(excelworksheet, "av2", "ay2", "Прооперировано больных туберкулезом", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // органов дыхания
                setRangeValue(excelworksheet, "av3", "aw3", "органов дыхания", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "av4", "av4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // %
                setRangeValue(excelworksheet, "aw4", "aw4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter);


                // БК(+)
                setRangeValue(excelworksheet, "ax3", "ay3", "внелегочными формами", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

                // абс.
                setRangeValue(excelworksheet, "ax4", "ax4", "абс.", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                // %
                setRangeValue(excelworksheet, "ay4", "ay4", "%", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string res = "";
                    char col = 'A';
                    col--;
                    
                    string fieldName = "";
                    string excelCell = "";

                    
                    for (int j = 0; j < 51; j++)
                    {
                        if (j < 26)
                        {
                            col++;       
                            excelCell = col.ToString();
                        }
                        else
                        {
                            col = 'A';
                            excelCell = col.ToString() + (char)(j - 26 + 'A');
                        }

                        res += excelCell + ' ';

                        excelcells = excelworksheet.get_Range(excelCell + (i + vertShift).ToString());

                        switch (dt.Columns["A" + j.ToString()].DataType.FullName)
                        {
                            case "System.Int32":
                                excelcells.Value = dt.Rows[i].Field<int>("A" + j.ToString());
                                break;
                            case "System.String":
                                excelcells.Value = dt.Rows[i].Field<string>("A" + j.ToString());
                                break;
                            case "System.Double":
                                excelcells.Value = dt.Rows[i].Field<double>("A" + j.ToString());

                                if (p.Contains(j))
                                    excelcells.NumberFormat = "0.00";
                                else
                                    excelcells.NumberFormat = "0";
                                break;
                        }

                        
                        
                    }
                    
                    //MessageBox.Show(res);
                }

                excelcells = excelworksheet.get_Range("A1", "AY" + (dt.Rows.Count + vertShift - 1).ToString());

                excelcells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                excelapp.Visible = true;
            //}
            
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void empСheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (empСheckBox.Checked)
                this.сотрудникTableAdapter1.Fill(tuberStatisticDBDataSet.Сотрудник);
                    else empComboBox.SelectedValue = -1;
            
            empComboBox.Enabled = empСheckBox.Checked;
        }

    }
}
