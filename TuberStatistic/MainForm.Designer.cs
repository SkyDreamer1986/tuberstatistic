﻿namespace TuberStatistic
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.mainMenu = new System.Windows.Forms.MenuStrip();
            this.manageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enterDataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.constIndMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.referenceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indicatorMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.territoriesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.structTypeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonInputReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tuberReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commonReportMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.docsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutFormMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mainMenuImageList = new System.Windows.Forms.ImageList(this.components);
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.кодDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.годDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кварталDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.структурнаяЕдиницаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tuberStatisticDBDataSet1 = new TuberStatistic.TuberStatisticDBDataSet();
            this.formsMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.формаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tuberStatisticDBDataSet = new TuberStatistic.TuberStatisticDBDataSet();
            this.fillByShortToolStrip = new System.Windows.Forms.ToolStrip();
            this.fillByShortToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.mainTabControl = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.refreshButton = new System.Windows.Forms.ToolStripButton();
            this.filterDataFormButton = new System.Windows.Forms.ToolStripButton();
            this.delDataFormFilterButton = new System.Windows.Forms.ToolStripButton();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.toolStrip2 = new System.Windows.Forms.ToolStrip();
            this.refreshConstDataButton = new System.Windows.Forms.ToolStripButton();
            this.filterConstFormButton = new System.Windows.Forms.ToolStripButton();
            this.delFilterConstButton = new System.Windows.Forms.ToolStripButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.кодDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.годDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кварталDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.constFormMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addConstFormMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editConstFormMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteConstFormMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printConstFormMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.формаПостоянныхBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.формаTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter();
            this.структурнаяЕдиницаTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter();
            this.оперативныйУчетTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ОперативныйУчетTableAdapter();
            this.формаПостоянныхTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter();
            this.tuberStatisticDBDataSet2 = new TuberStatistic.TuberStatisticDBDataSet();
            this.формаПостоянныхBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.постоянныеТерриторияTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ПостоянныеТерриторияTableAdapter();
            this.формаПостоянныхTableAdapter1 = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter();
            this.mainMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet1)).BeginInit();
            this.formsMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.формаBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).BeginInit();
            this.fillByShortToolStrip.SuspendLayout();
            this.mainTabControl.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.constFormMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.формаПостоянныхBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.формаПостоянныхBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 572);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(897, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // mainMenu
            // 
            this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manageMenuItem,
            this.operationMenuItem,
            this.reportMenuItem,
            this.referenceMenuItem,
            this.docsMenuItem});
            this.mainMenu.Location = new System.Drawing.Point(0, 0);
            this.mainMenu.Name = "mainMenu";
            this.mainMenu.Size = new System.Drawing.Size(897, 24);
            this.mainMenu.TabIndex = 2;
            this.mainMenu.Text = "menuStrip1";
            this.mainMenu.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mainMenu_ItemClicked);
            // 
            // manageMenuItem
            // 
            this.manageMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitMenuItem});
            this.manageMenuItem.Name = "manageMenuItem";
            this.manageMenuItem.Size = new System.Drawing.Size(85, 20);
            this.manageMenuItem.Text = "Управление";
            this.manageMenuItem.Visible = false;
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(108, 22);
            this.exitMenuItem.Text = "Выход";
            this.exitMenuItem.Click += new System.EventHandler(this.exitMenuItem_Click);
            // 
            // operationMenuItem
            // 
            this.operationMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.enterDataMenuItem,
            this.constIndMenuItem});
            this.operationMenuItem.Name = "operationMenuItem";
            this.operationMenuItem.Size = new System.Drawing.Size(88, 20);
            this.operationMenuItem.Text = "Ввод данных";
            // 
            // enterDataMenuItem
            // 
            this.enterDataMenuItem.Name = "enterDataMenuItem";
            this.enterDataMenuItem.Size = new System.Drawing.Size(208, 22);
            this.enterDataMenuItem.Text = "Оперативный учет";
            this.enterDataMenuItem.Click += new System.EventHandler(this.enterDataMenuItem_Click);
            // 
            // constIndMenuItem
            // 
            this.constIndMenuItem.Name = "constIndMenuItem";
            this.constIndMenuItem.Size = new System.Drawing.Size(208, 22);
            this.constIndMenuItem.Text = "Постоянные показатели";
            this.constIndMenuItem.Click += new System.EventHandler(this.addConstFormMenuItem_Click);
            // 
            // referenceMenuItem
            // 
            this.referenceMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.indicatorMenuItem,
            this.territoriesMenuItem,
            this.structTypeMenuItem,
            this.empMenuItem});
            this.referenceMenuItem.Name = "referenceMenuItem";
            this.referenceMenuItem.Size = new System.Drawing.Size(94, 20);
            this.referenceMenuItem.Text = "Справочники";
            // 
            // indicatorMenuItem
            // 
            this.indicatorMenuItem.Name = "indicatorMenuItem";
            this.indicatorMenuItem.Size = new System.Drawing.Size(172, 22);
            this.indicatorMenuItem.Text = "Показатели";
            this.indicatorMenuItem.Click += new System.EventHandler(this.indicatorMenuItem_Click);
            // 
            // territoriesMenuItem
            // 
            this.territoriesMenuItem.Name = "territoriesMenuItem";
            this.territoriesMenuItem.Size = new System.Drawing.Size(172, 22);
            this.territoriesMenuItem.Text = "Территории";
            this.territoriesMenuItem.Click += new System.EventHandler(this.territoriesMenuItem_Click);
            // 
            // structTypeMenuItem
            // 
            this.structTypeMenuItem.Name = "structTypeMenuItem";
            this.structTypeMenuItem.Size = new System.Drawing.Size(172, 22);
            this.structTypeMenuItem.Text = "Типы территорий";
            this.structTypeMenuItem.Click += new System.EventHandler(this.structTypeMenuItem_Click);
            // 
            // empMenuItem
            // 
            this.empMenuItem.Name = "empMenuItem";
            this.empMenuItem.Size = new System.Drawing.Size(172, 22);
            this.empMenuItem.Text = "Сотрудники";
            this.empMenuItem.Click += new System.EventHandler(this.empMenuItem_Click);
            // 
            // reportMenuItem
            // 
            this.reportMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commonInputReportMenuItem,
            this.tuberReportMenuItem,
            this.commonReportMenuItem});
            this.reportMenuItem.Name = "reportMenuItem";
            this.reportMenuItem.Size = new System.Drawing.Size(60, 20);
            this.reportMenuItem.Text = "Отчеты";
            // 
            // commonInputReportMenuItem
            // 
            this.commonInputReportMenuItem.Name = "commonInputReportMenuItem";
            this.commonInputReportMenuItem.Size = new System.Drawing.Size(332, 22);
            this.commonInputReportMenuItem.Text = "Сводный отчет по территориям";
            this.commonInputReportMenuItem.Click += new System.EventHandler(this.commonInputReportMenuItem_Click);
            // 
            // tuberReportMenuItem
            // 
            this.tuberReportMenuItem.Name = "tuberReportMenuItem";
            this.tuberReportMenuItem.Size = new System.Drawing.Size(332, 22);
            this.tuberReportMenuItem.Text = "Туберкулинодиагностика";
            this.tuberReportMenuItem.Click += new System.EventHandler(this.tuberReportMenuItem_Click);
            // 
            // commonReportMenuItem
            // 
            this.commonReportMenuItem.Name = "commonReportMenuItem";
            this.commonReportMenuItem.Size = new System.Drawing.Size(332, 22);
            this.commonReportMenuItem.Text = "Сводный отчет противотуберкулезной работы";
            this.commonReportMenuItem.Click += new System.EventHandler(this.commonReportMenuItem_Click);
            // 
            // docsMenuItem
            // 
            this.docsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutFormMenuItem});
            this.docsMenuItem.Name = "docsMenuItem";
            this.docsMenuItem.Size = new System.Drawing.Size(65, 20);
            this.docsMenuItem.Text = "Справка";
            // 
            // aboutFormMenuItem
            // 
            this.aboutFormMenuItem.Name = "aboutFormMenuItem";
            this.aboutFormMenuItem.Size = new System.Drawing.Size(149, 22);
            this.aboutFormMenuItem.Text = "О программе";
            this.aboutFormMenuItem.Click += new System.EventHandler(this.aboutFormMenuItem_Click);
            // 
            // mainMenuImageList
            // 
            this.mainMenuImageList.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.mainMenuImageList.ImageSize = new System.Drawing.Size(16, 16);
            this.mainMenuImageList.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодDataGridViewTextBoxColumn,
            this.годDataGridViewTextBoxColumn,
            this.кварталDataGridViewTextBoxColumn,
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn});
            this.dataGridView1.ContextMenuStrip = this.formsMenuStrip;
            this.dataGridView1.DataSource = this.формаBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(8, 33);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(873, 480);
            this.dataGridView1.TabIndex = 3;
            this.dataGridView1.DoubleClick += new System.EventHandler(this.editStripMenuItem_Click);
            // 
            // кодDataGridViewTextBoxColumn
            // 
            this.кодDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодDataGridViewTextBoxColumn.DataPropertyName = "Код";
            this.кодDataGridViewTextBoxColumn.HeaderText = "Код";
            this.кодDataGridViewTextBoxColumn.Name = "кодDataGridViewTextBoxColumn";
            this.кодDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.кодDataGridViewTextBoxColumn.Width = 80;
            // 
            // годDataGridViewTextBoxColumn
            // 
            this.годDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.годDataGridViewTextBoxColumn.DataPropertyName = "Год";
            this.годDataGridViewTextBoxColumn.FillWeight = 82.05128F;
            this.годDataGridViewTextBoxColumn.HeaderText = "Год";
            this.годDataGridViewTextBoxColumn.Name = "годDataGridViewTextBoxColumn";
            this.годDataGridViewTextBoxColumn.ReadOnly = true;
            this.годDataGridViewTextBoxColumn.Width = 80;
            // 
            // кварталDataGridViewTextBoxColumn
            // 
            this.кварталDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кварталDataGridViewTextBoxColumn.DataPropertyName = "Квартал";
            this.кварталDataGridViewTextBoxColumn.FillWeight = 205.1282F;
            this.кварталDataGridViewTextBoxColumn.HeaderText = "Квартал";
            this.кварталDataGridViewTextBoxColumn.Name = "кварталDataGridViewTextBoxColumn";
            this.кварталDataGridViewTextBoxColumn.ReadOnly = true;
            this.кварталDataGridViewTextBoxColumn.Width = 80;
            // 
            // кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn
            // 
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DataPropertyName = "Код_СтруктурнаяЕдиница";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DataSource = this.структурнаяЕдиницаBindingSource;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.FillWeight = 12.82051F;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.HeaderText = "Территория";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.Name = "кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn";
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.ReadOnly = true;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn.ValueMember = "Код";
            // 
            // структурнаяЕдиницаBindingSource
            // 
            this.структурнаяЕдиницаBindingSource.DataMember = "СтруктурнаяЕдиница";
            this.структурнаяЕдиницаBindingSource.DataSource = this.tuberStatisticDBDataSet1;
            // 
            // tuberStatisticDBDataSet1
            // 
            this.tuberStatisticDBDataSet1.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // formsMenuStrip
            // 
            this.formsMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMenuItem,
            this.editStripMenuItem,
            this.deleteStripMenuItem,
            this.printMenuItem});
            this.formsMenuStrip.Name = "formsMenuStrip";
            this.formsMenuStrip.Size = new System.Drawing.Size(155, 92);
            // 
            // addMenuItem
            // 
            this.addMenuItem.Name = "addMenuItem";
            this.addMenuItem.Size = new System.Drawing.Size(154, 22);
            this.addMenuItem.Text = "Добавить";
            this.addMenuItem.Click += new System.EventHandler(this.enterDataMenuItem_Click);
            // 
            // editStripMenuItem
            // 
            this.editStripMenuItem.Name = "editStripMenuItem";
            this.editStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.editStripMenuItem.Text = "Редактировать";
            this.editStripMenuItem.Click += new System.EventHandler(this.editStripMenuItem_Click);
            // 
            // deleteStripMenuItem
            // 
            this.deleteStripMenuItem.Name = "deleteStripMenuItem";
            this.deleteStripMenuItem.Size = new System.Drawing.Size(154, 22);
            this.deleteStripMenuItem.Text = "Удалить";
            this.deleteStripMenuItem.Click += new System.EventHandler(this.deleteStripMenuItem_Click);
            // 
            // printMenuItem
            // 
            this.printMenuItem.Name = "printMenuItem";
            this.printMenuItem.Size = new System.Drawing.Size(154, 22);
            this.printMenuItem.Text = "Печать";
            this.printMenuItem.Click += new System.EventHandler(this.printMenuItem_Click);
            // 
            // формаBindingSource
            // 
            this.формаBindingSource.DataMember = "Форма";
            this.формаBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // tuberStatisticDBDataSet
            // 
            this.tuberStatisticDBDataSet.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fillByShortToolStrip
            // 
            this.fillByShortToolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fillByShortToolStripButton});
            this.fillByShortToolStrip.Location = new System.Drawing.Point(0, 24);
            this.fillByShortToolStrip.Name = "fillByShortToolStrip";
            this.fillByShortToolStrip.Size = new System.Drawing.Size(897, 25);
            this.fillByShortToolStrip.TabIndex = 4;
            this.fillByShortToolStrip.Text = "fillByShortToolStrip";
            this.fillByShortToolStrip.Visible = false;
            // 
            // fillByShortToolStripButton
            // 
            this.fillByShortToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.fillByShortToolStripButton.Name = "fillByShortToolStripButton";
            this.fillByShortToolStripButton.Size = new System.Drawing.Size(79, 22);
            this.fillByShortToolStripButton.Text = "Весь период";
            this.fillByShortToolStripButton.Click += new System.EventHandler(this.fillByShortToolStripButton_Click);
            // 
            // mainTabControl
            // 
            this.mainTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mainTabControl.Controls.Add(this.tabPage1);
            this.mainTabControl.Controls.Add(this.tabPage2);
            this.mainTabControl.Location = new System.Drawing.Point(0, 27);
            this.mainTabControl.Name = "mainTabControl";
            this.mainTabControl.SelectedIndex = 0;
            this.mainTabControl.Size = new System.Drawing.Size(897, 542);
            this.mainTabControl.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.toolStrip1);
            this.tabPage1.Controls.Add(this.dataGridView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(889, 516);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Противотуберкулезная работа на территориях";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshButton,
            this.filterDataFormButton,
            this.delDataFormFilterButton});
            this.toolStrip1.Location = new System.Drawing.Point(3, 3);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(883, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // refreshButton
            // 
            this.refreshButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.refreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshButton.Image")));
            this.refreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(23, 22);
            this.refreshButton.Text = "toolStripButton4";
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // filterDataFormButton
            // 
            this.filterDataFormButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.filterDataFormButton.Image = ((System.Drawing.Image)(resources.GetObject("filterDataFormButton.Image")));
            this.filterDataFormButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filterDataFormButton.Name = "filterDataFormButton";
            this.filterDataFormButton.Size = new System.Drawing.Size(23, 22);
            this.filterDataFormButton.Text = "Фильтр";
            this.filterDataFormButton.Click += new System.EventHandler(this.filterDataFormButton_Click);
            // 
            // delDataFormFilterButton
            // 
            this.delDataFormFilterButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.delDataFormFilterButton.Image = ((System.Drawing.Image)(resources.GetObject("delDataFormFilterButton.Image")));
            this.delDataFormFilterButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delDataFormFilterButton.Name = "delDataFormFilterButton";
            this.delDataFormFilterButton.Size = new System.Drawing.Size(23, 22);
            this.delDataFormFilterButton.Text = "Удалить фильтр";
            this.delDataFormFilterButton.Click += new System.EventHandler(this.delDataFormFilterButton_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.toolStrip2);
            this.tabPage2.Controls.Add(this.dataGridView2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(889, 516);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Постоянные показатели территорий";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // toolStrip2
            // 
            this.toolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshConstDataButton,
            this.filterConstFormButton,
            this.delFilterConstButton});
            this.toolStrip2.Location = new System.Drawing.Point(3, 3);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.Size = new System.Drawing.Size(883, 25);
            this.toolStrip2.TabIndex = 6;
            this.toolStrip2.Text = "toolStrip2";
            // 
            // refreshConstDataButton
            // 
            this.refreshConstDataButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.refreshConstDataButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.refreshConstDataButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshConstDataButton.Image")));
            this.refreshConstDataButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshConstDataButton.Name = "refreshConstDataButton";
            this.refreshConstDataButton.Size = new System.Drawing.Size(23, 22);
            this.refreshConstDataButton.Text = "Обновить";
            this.refreshConstDataButton.Click += new System.EventHandler(this.refreshConstDataButton_Click);
            // 
            // filterConstFormButton
            // 
            this.filterConstFormButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.filterConstFormButton.Image = ((System.Drawing.Image)(resources.GetObject("filterConstFormButton.Image")));
            this.filterConstFormButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.filterConstFormButton.Name = "filterConstFormButton";
            this.filterConstFormButton.Size = new System.Drawing.Size(23, 22);
            this.filterConstFormButton.Text = "Фильтрация";
            this.filterConstFormButton.Click += new System.EventHandler(this.filterConstFormButton_Click);
            // 
            // delFilterConstButton
            // 
            this.delFilterConstButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.delFilterConstButton.Image = ((System.Drawing.Image)(resources.GetObject("delFilterConstButton.Image")));
            this.delFilterConstButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.delFilterConstButton.Name = "delFilterConstButton";
            this.delFilterConstButton.Size = new System.Drawing.Size(23, 22);
            this.delFilterConstButton.Text = "Удалить фильтр";
            this.delFilterConstButton.Click += new System.EventHandler(this.delFilterConstButton_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодDataGridViewTextBoxColumn1,
            this.годDataGridViewTextBoxColumn1,
            this.кварталDataGridViewTextBoxColumn1});
            this.dataGridView2.ContextMenuStrip = this.constFormMenuStrip;
            this.dataGridView2.DataSource = this.формаПостоянныхBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(8, 33);
            this.dataGridView2.MultiSelect = false;
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2.Size = new System.Drawing.Size(873, 480);
            this.dataGridView2.TabIndex = 5;
            this.dataGridView2.DoubleClick += new System.EventHandler(this.editConstFormMenuItem_Click);
            // 
            // кодDataGridViewTextBoxColumn1
            // 
            this.кодDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодDataGridViewTextBoxColumn1.DataPropertyName = "Код";
            this.кодDataGridViewTextBoxColumn1.HeaderText = "Код";
            this.кодDataGridViewTextBoxColumn1.Name = "кодDataGridViewTextBoxColumn1";
            this.кодDataGridViewTextBoxColumn1.ReadOnly = true;
            this.кодDataGridViewTextBoxColumn1.Width = 80;
            // 
            // годDataGridViewTextBoxColumn1
            // 
            this.годDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.годDataGridViewTextBoxColumn1.DataPropertyName = "Год";
            this.годDataGridViewTextBoxColumn1.HeaderText = "Год";
            this.годDataGridViewTextBoxColumn1.Name = "годDataGridViewTextBoxColumn1";
            this.годDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // кварталDataGridViewTextBoxColumn1
            // 
            this.кварталDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.кварталDataGridViewTextBoxColumn1.DataPropertyName = "Квартал";
            this.кварталDataGridViewTextBoxColumn1.HeaderText = "Квартал";
            this.кварталDataGridViewTextBoxColumn1.Name = "кварталDataGridViewTextBoxColumn1";
            this.кварталDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // constFormMenuStrip
            // 
            this.constFormMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addConstFormMenuItem,
            this.editConstFormMenuItem,
            this.deleteConstFormMenuItem,
            this.printConstFormMenuItem});
            this.constFormMenuStrip.Name = "constFormMenuStrip";
            this.constFormMenuStrip.Size = new System.Drawing.Size(155, 92);
            // 
            // addConstFormMenuItem
            // 
            this.addConstFormMenuItem.Name = "addConstFormMenuItem";
            this.addConstFormMenuItem.Size = new System.Drawing.Size(154, 22);
            this.addConstFormMenuItem.Text = "Добавить";
            this.addConstFormMenuItem.Click += new System.EventHandler(this.addConstFormMenuItem_Click);
            // 
            // editConstFormMenuItem
            // 
            this.editConstFormMenuItem.Name = "editConstFormMenuItem";
            this.editConstFormMenuItem.Size = new System.Drawing.Size(154, 22);
            this.editConstFormMenuItem.Text = "Редактировать";
            this.editConstFormMenuItem.Click += new System.EventHandler(this.editConstFormMenuItem_Click);
            // 
            // deleteConstFormMenuItem
            // 
            this.deleteConstFormMenuItem.Name = "deleteConstFormMenuItem";
            this.deleteConstFormMenuItem.Size = new System.Drawing.Size(154, 22);
            this.deleteConstFormMenuItem.Text = "Удалить";
            this.deleteConstFormMenuItem.Click += new System.EventHandler(this.deleteConstFormMenuItem_Click);
            // 
            // printConstFormMenuItem
            // 
            this.printConstFormMenuItem.Name = "printConstFormMenuItem";
            this.printConstFormMenuItem.Size = new System.Drawing.Size(154, 22);
            this.printConstFormMenuItem.Text = "Печать";
            this.printConstFormMenuItem.Click += new System.EventHandler(this.printConstFormMenuItem_Click);
            // 
            // формаПостоянныхBindingSource
            // 
            this.формаПостоянныхBindingSource.DataMember = "ФормаПостоянных";
            this.формаПостоянныхBindingSource.DataSource = this.tuberStatisticDBDataSet1;
            // 
            // формаTableAdapter
            // 
            this.формаTableAdapter.ClearBeforeFill = true;
            // 
            // структурнаяЕдиницаTableAdapter
            // 
            this.структурнаяЕдиницаTableAdapter.ClearBeforeFill = true;
            // 
            // оперативныйУчетTableAdapter1
            // 
            this.оперативныйУчетTableAdapter1.ClearBeforeFill = true;
            // 
            // формаПостоянныхTableAdapter
            // 
            this.формаПостоянныхTableAdapter.ClearBeforeFill = true;
            // 
            // tuberStatisticDBDataSet2
            // 
            this.tuberStatisticDBDataSet2.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet2.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // формаПостоянныхBindingSource1
            // 
            this.формаПостоянныхBindingSource1.DataMember = "ФормаПостоянных";
            this.формаПостоянныхBindingSource1.DataSource = this.tuberStatisticDBDataSet2;
            // 
            // постоянныеТерриторияTableAdapter1
            // 
            this.постоянныеТерриторияTableAdapter1.ClearBeforeFill = true;
            // 
            // формаПостоянныхTableAdapter1
            // 
            this.формаПостоянныхTableAdapter1.ClearBeforeFill = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(897, 594);
            this.Controls.Add(this.mainTabControl);
            this.Controls.Add(this.fillByShortToolStrip);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.mainMenu);
            this.MainMenuStrip = this.mainMenu;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Централизованный оперативный учет";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.mainMenu.ResumeLayout(false);
            this.mainMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet1)).EndInit();
            this.formsMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.формаBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).EndInit();
            this.fillByShortToolStrip.ResumeLayout(false);
            this.fillByShortToolStrip.PerformLayout();
            this.mainTabControl.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.constFormMenuStrip.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.формаПостоянныхBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.формаПостоянныхBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip mainMenu;
        private System.Windows.Forms.ToolStripMenuItem manageMenuItem;
        private System.Windows.Forms.ToolStripMenuItem operationMenuItem;
        private System.Windows.Forms.ToolStripMenuItem referenceMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reportMenuItem;
        private System.Windows.Forms.ImageList mainMenuImageList;
        private System.Windows.Forms.ToolStripMenuItem enterDataMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indicatorMenuItem;
        private System.Windows.Forms.ToolStripMenuItem structTypeMenuItem;
        private System.Windows.Forms.ToolStripMenuItem territoriesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empMenuItem;
        private System.Windows.Forms.DataGridView dataGridView1;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet;
        private System.Windows.Forms.BindingSource формаBindingSource;
        private System.Windows.Forms.ToolStrip fillByShortToolStrip;
        private System.Windows.Forms.ToolStripButton fillByShortToolStripButton;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet1;
        private System.Windows.Forms.BindingSource структурнаяЕдиницаBindingSource;
        private TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter структурнаяЕдиницаTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem docsMenuItem;
        private System.Windows.Forms.ContextMenuStrip formsMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteStripMenuItem;
        private TuberStatisticDBDataSetTableAdapters.ОперативныйУчетTableAdapter оперативныйУчетTableAdapter1;
        public TuberStatisticDBDataSetTableAdapters.ФормаTableAdapter формаTableAdapter;
        private System.Windows.Forms.ToolStripMenuItem addMenuItem;
        private System.Windows.Forms.ToolStripMenuItem constIndMenuItem;
        private System.Windows.Forms.TabControl mainTabControl;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.ToolStrip toolStrip2;
        private System.Windows.Forms.BindingSource формаПостоянныхBindingSource;
        private TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter формаПостоянныхTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn годDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn кварталDataGridViewTextBoxColumn1;
        private System.Windows.Forms.ContextMenuStrip constFormMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addConstFormMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editConstFormMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteConstFormMenuItem;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet2;
        private System.Windows.Forms.BindingSource формаПостоянныхBindingSource1;
        private TuberStatisticDBDataSetTableAdapters.ПостоянныеТерриторияTableAdapter постоянныеТерриторияTableAdapter1;
        private TuberStatisticDBDataSetTableAdapters.ФормаПостоянныхTableAdapter формаПостоянныхTableAdapter1;
        private System.Windows.Forms.ToolStripMenuItem tuberReportMenuItem;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn годDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn кварталDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодСтруктурнаяЕдиницаDataGridViewTextBoxColumn;
        private System.Windows.Forms.ToolStripButton filterDataFormButton;
        private System.Windows.Forms.ToolStripButton delDataFormFilterButton;
        private System.Windows.Forms.ToolStripButton filterConstFormButton;
        private System.Windows.Forms.ToolStripButton delFilterConstButton;
        private System.Windows.Forms.ToolStripButton refreshButton;
        private System.Windows.Forms.ToolStripButton refreshConstDataButton;
        private System.Windows.Forms.ToolStripMenuItem commonReportMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printConstFormMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutFormMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commonInputReportMenuItem;
    }
}

