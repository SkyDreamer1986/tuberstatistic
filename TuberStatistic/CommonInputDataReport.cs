﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace TuberStatistic
{
    public partial class CommonInputDataReport : Form
    {
        public CommonInputDataReport()
        {
            InitializeComponent();
            
        }

        private void CommonInputDataReport_Load(object sender, EventArgs e)
        {
            this.типСтруктурнойЕдиницыTableAdapter1.Fill(tuberStatisticDBDataSet.ТипСтруктурнойЕдиницы);

            yearUpDown1.Value = yearUpDown1.Minimum = (int)this.формаTableAdapter1.GetLastYear();
            yearUpDown1.Maximum = (int)this.формаTableAdapter1.GetLastYear();
            yearUpDown1.Minimum = (int)this.формаTableAdapter1.GetFirstYear();
        }

        Excel.Application excelapp;
        Excel.Sheets excelsheets;
        Excel.Worksheet excelworksheet;
        Excel.Range excelcells;
        Excel.Workbook excelappworkbook;

        int vertShift = 6;
        const int defWidth = 12;

        private void setRangeValue(Excel.Worksheet excelworksheet, string rangeFrom, string rangeTo, string Value, Excel.Constants horizAlignment, Excel.Constants vertAlignment, int cellWidth = 0)
        {
            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper());
            excelcells.Value = Value;

            excelcells = excelworksheet.get_Range(rangeFrom.ToUpper(), rangeTo.ToUpper());
            excelcells.Select();
            ((Excel.Range)(excelapp.Selection)).Merge(Type.Missing);
            excelcells.HorizontalAlignment = Excel.Constants.xlCenter;
            excelcells.VerticalAlignment = Excel.Constants.xlCenter;

            if (cellWidth > 0) excelcells.EntireColumn.ColumnWidth = cellWidth;
            excelcells.WrapText = true;
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            int? territoryKind = (int)territoryKindComboBox.SelectedValue == 3 ? null : (int?)territoryKindComboBox.SelectedValue;

            DataTable dt = формаTableAdapter1.GetDataByCommonInputData((int)yearUpDown1.Value, (int)quarterUpDown1.Value, territoryKind);

            if (dt.Rows.Count == 0)
            {
                MessageBox.Show("Данные для построения отчета отсутствуют!");
                return;
            }

            excelapp = new Excel.Application();

            excelapp.Workbooks.Add(Type.Missing);

            excelappworkbook = excelapp.Workbooks[1];

            excelsheets = excelappworkbook.Worksheets;

            //Получаем ссылку на лист 1
            excelworksheet = (Excel.Worksheet)excelsheets.get_Item(1);

            // Заголовок

            string repTerritory = "по области";

            setRangeValue(excelworksheet, "A1", "g1", "Централизованный оперативный учет противотуберкулезной работы " , Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            setRangeValue(excelworksheet, "a2", "d3", "", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            // Территория
            string ter = "Область";

            if (territoryKind == 1)
                ter = "Города";
            else
                if (territoryKind == 2) ter = "Районы";

            //ter = ter.Substring(0, 2) == "г." ? ter : ter + " район";

            setRangeValue(excelworksheet, "e2", "g2", ter, Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            // Дата
            setRangeValue(excelworksheet, "e3", "g3", quarterUpDown1.Value.ToString() + " квартал " + yearUpDown1.Value.ToString(), Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            // Колонка данных прошлого года этого же периода
            setRangeValue(excelworksheet, "A4", "c4", "За этот же период прошлого года", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            setRangeValue(excelworksheet, "a5", "a5", "взрослые", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "b5", "b5", "подростки", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "c5", "c5", "дети", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            // Наименование показателя
            setRangeValue(excelworksheet, "d4", "d4", "Показатели", Excel.Constants.xlCenter, Excel.Constants.xlCenter, 80);

            // Текущие данные
            setRangeValue(excelworksheet, "e4", "g4", "Текущие значения", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            setRangeValue(excelworksheet, "e5", "e5", "взрослые", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "f5", "f5", "подростки", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "g5", "g5", "дети", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                excelcells = excelworksheet.get_Range('A' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("lastyear.ВеличинаВзрослые");

                excelcells = excelworksheet.get_Range('B' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("lastyear.ВеличинаПодростки");

                excelcells = excelworksheet.get_Range('C' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("lastyear.ВеличинаДети");

                excelcells = excelworksheet.get_Range('D' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<string>("Наименование");

                excelcells = excelworksheet.get_Range('E' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("currentyear.ВеличинаВзрослые");

                excelcells = excelworksheet.get_Range('F' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("currentyear.ВеличинаПодростки");

                excelcells = excelworksheet.get_Range('G' + (i + vertShift).ToString());
                excelcells.Value = dt.Rows[i].Field<double?>("currentyear.ВеличинаДети");
            }

            vertShift = vertShift + dt.Rows.Count + 1;


            setRangeValue(excelworksheet, "a" + vertShift.ToString(), "a" + vertShift.ToString(), "Фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "b" + vertShift.ToString(), "b" + vertShift.ToString(), "Детские фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

            // Наименование показателя
            setRangeValue(excelworksheet, "c" + vertShift.ToString(), "e" + vertShift.ToString(), "Штаты фтизиатров", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            // Текущие данные
            setRangeValue(excelworksheet, "f" + vertShift.ToString(), "f" + vertShift.ToString(), "Фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);
            setRangeValue(excelworksheet, "g" + vertShift.ToString(), "g" + vertShift.ToString(), "Детские фтизиатры", Excel.Constants.xlCenter, Excel.Constants.xlCenter, defWidth);

            setRangeValue(excelworksheet, "c" + (vertShift + 1).ToString(), "d" + (vertShift + 2).ToString(), "Штатных должностей", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            setRangeValue(excelworksheet, "e" + (vertShift + 1).ToString(), "e" + (vertShift + 1).ToString(), "всего", Excel.Constants.xlCenter, Excel.Constants.xlCenter);
            setRangeValue(excelworksheet, "e" + (vertShift + 2).ToString(), "e" + (vertShift + 2).ToString(), "занято", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            setRangeValue(excelworksheet, "c" + (vertShift + 3).ToString(), "e" + (vertShift + 3).ToString(), "Физических лиц", Excel.Constants.xlCenter, Excel.Constants.xlCenter);

            dt = формаTableAdapter1.GetDataByCommonStaff((int)yearUpDown1.Value, (int)quarterUpDown1.Value, territoryKind);

            excelcells = excelworksheet.get_Range('A' + (vertShift + 1).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ШтатныхДолжностейВсегоФ");

            excelcells = excelworksheet.get_Range('A' + (vertShift + 2).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ШтатныхДолжностейЗанятоФ");

            excelcells = excelworksheet.get_Range('B' + (vertShift + 1).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ШтатныхДолжностейВсегоФД");

            excelcells = excelworksheet.get_Range('B' + (vertShift + 2).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ШтатныхДолжностейЗанятоФД");

            excelcells = excelworksheet.get_Range('A' + (vertShift + 3).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ФизическихЛицФ");

            excelcells = excelworksheet.get_Range('B' + (vertShift + 3).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф2_ФизическихЛицФД");

            
            excelcells = excelworksheet.get_Range('F' + (vertShift + 1).ToString());
            excelcells.Value = (dt.Rows[0].Field<double?>("ф_ШтатныхДолжностейВсегоФ")).ToString();

            excelcells = excelworksheet.get_Range('F' + (vertShift + 2).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф_ШтатныхДолжностейЗанятоФ");

            excelcells = excelworksheet.get_Range('G' + (vertShift + 1).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф_ШтатныхДолжностейВсегоФД");

            excelcells = excelworksheet.get_Range('G' + (vertShift + 2).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф_ШтатныхДолжностейЗанятоФД");

            excelcells = excelworksheet.get_Range('F' + (vertShift + 3).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф_ФизическихЛицФ");

            excelcells = excelworksheet.get_Range('G' + (vertShift + 3).ToString());
            excelcells.Value = dt.Rows[0].Field<double?>("ф_ФизическихЛицФД");

            excelcells = excelworksheet.get_Range("A1", "G" + (vertShift + 3).ToString());

            excelcells.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

            setRangeValue(excelworksheet, "A" + (vertShift + 4).ToString(), 'G' + (vertShift + 5).ToString(), "*Примечание: показатели, вошедшие в \"Центральный оперативный учет\""
                    + "взяты из отчетных форм №30 и 33, утвержденных постановлением Госкомстата России №175 от 10.09.2002 г."
                    + "и постановлением Росстата №80 от 11.11.2005 г.", Excel.Constants.xlLeft, Excel.Constants.xlLeft);

            excelcells = excelworksheet.get_Range('E' + (vertShift + 6).ToString());
            excelcells.Value = "Должность";

            excelcells = excelworksheet.get_Range('F' + (vertShift + 6).ToString());
            excelcells.Value = "___________________";

            excelcells = excelworksheet.get_Range('E' + (vertShift + 7).ToString());
            excelcells.Value = "Подпись";

            excelcells = excelworksheet.get_Range('F' + (vertShift + 7).ToString());
            excelcells.Value = "___________________";



            excelapp.Visible = true;
        }
    }
}
