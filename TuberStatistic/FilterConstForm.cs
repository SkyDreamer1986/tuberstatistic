﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TuberStatistic
{
    public partial class FilterConstForm : Form
    {
        public FilterConstForm()
        {
            InitializeComponent();
        }

        private void FilterConstForm_Load(object sender, EventArgs e)
        {
            int? ys = формаПостоянныхTableAdapter1.GetFirstYear();
            int? ye = формаПостоянныхTableAdapter1.GetLastYear();

            yearUpDown1.Minimum = yearUpDown2.Minimum = (int)ys;
            yearUpDown1.Maximum = yearUpDown2.Maximum = (int)ye;

            yearUpDown2.Value = yearUpDown1.Value = (int)ye;
            
            yearCheckBox.Checked = true;

            quarterCheckBox.Checked = true;
            quarterCheckBox.Checked = false;
        }

        private void quarterCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            quarterUpDown1.Enabled = quarterCheckBox.Checked;
            quarterUpDown2.Enabled = quarterCheckBox.Checked;
        }

        private void okButton_Click(object sender, EventArgs e)
        {

        }
    }
}
