﻿namespace TuberStatistic
{
    partial class TerritoriesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.кодDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.наименованиеDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.типСтруктурнойЕдиницыBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tuberStatisticDBDataSet = new TuberStatistic.TuberStatisticDBDataSet();
            this.кодСотрудникDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.сотрудникBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.структурнаяЕдиницаBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.типСтруктурнойЕдиницыBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.сотрудникBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.структурнаяЕдиницаTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter();
            this.типСтруктурнойЕдиницыTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.ТипСтруктурнойЕдиницыTableAdapter();
            this.сотрудникTableAdapter = new TuberStatistic.TuberStatisticDBDataSetTableAdapters.СотрудникTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.типСтруктурнойЕдиницыBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.типСтруктурнойЕдиницыBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.кодDataGridViewTextBoxColumn,
            this.наименованиеDataGridViewTextBoxColumn,
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn,
            this.кодСотрудникDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.структурнаяЕдиницаBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(0, -1);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(742, 418);
            this.dataGridView1.TabIndex = 1;
            // 
            // кодDataGridViewTextBoxColumn
            // 
            this.кодDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодDataGridViewTextBoxColumn.DataPropertyName = "Код";
            this.кодDataGridViewTextBoxColumn.HeaderText = "Код";
            this.кодDataGridViewTextBoxColumn.Name = "кодDataGridViewTextBoxColumn";
            this.кодDataGridViewTextBoxColumn.Width = 80;
            // 
            // наименованиеDataGridViewTextBoxColumn
            // 
            this.наименованиеDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.наименованиеDataGridViewTextBoxColumn.DataPropertyName = "Наименование";
            this.наименованиеDataGridViewTextBoxColumn.HeaderText = "Наименование";
            this.наименованиеDataGridViewTextBoxColumn.Name = "наименованиеDataGridViewTextBoxColumn";
            // 
            // кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn
            // 
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.DataPropertyName = "Код_ТипСтруктурнойЕдиницы";
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.DataSource = this.типСтруктурнойЕдиницыBindingSource;
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.DisplayMember = "Наименование";
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.HeaderText = "Тип";
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.Name = "кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn";
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn.ValueMember = "Код";
            // 
            // типСтруктурнойЕдиницыBindingSource
            // 
            this.типСтруктурнойЕдиницыBindingSource.DataMember = "ТипСтруктурнойЕдиницы";
            this.типСтруктурнойЕдиницыBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // tuberStatisticDBDataSet
            // 
            this.tuberStatisticDBDataSet.DataSetName = "TuberStatisticDBDataSet";
            this.tuberStatisticDBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // кодСотрудникDataGridViewTextBoxColumn
            // 
            this.кодСотрудникDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.кодСотрудникDataGridViewTextBoxColumn.DataPropertyName = "Код_Сотрудник";
            this.кодСотрудникDataGridViewTextBoxColumn.DataSource = this.сотрудникBindingSource;
            this.кодСотрудникDataGridViewTextBoxColumn.DisplayMember = "ФИО";
            this.кодСотрудникDataGridViewTextBoxColumn.HeaderText = "Сотрудник";
            this.кодСотрудникDataGridViewTextBoxColumn.Name = "кодСотрудникDataGridViewTextBoxColumn";
            this.кодСотрудникDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.кодСотрудникDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.кодСотрудникDataGridViewTextBoxColumn.ValueMember = "Код";
            // 
            // сотрудникBindingSource
            // 
            this.сотрудникBindingSource.DataMember = "Сотрудник";
            this.сотрудникBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // структурнаяЕдиницаBindingSource
            // 
            this.структурнаяЕдиницаBindingSource.DataMember = "СтруктурнаяЕдиница";
            this.структурнаяЕдиницаBindingSource.DataSource = this.tuberStatisticDBDataSet;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.ForeColor = System.Drawing.Color.DimGray;
            this.panel1.Location = new System.Drawing.Point(0, 423);
            this.panel1.Name = "panel1";
            this.panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.panel1.Size = new System.Drawing.Size(742, 49);
            this.panel1.TabIndex = 12;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.ForeColor = System.Drawing.Color.Black;
            this.cancelButton.Location = new System.Drawing.Point(566, 16);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 10;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.ForeColor = System.Drawing.Color.Black;
            this.okButton.Location = new System.Drawing.Point(647, 16);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 9;
            this.okButton.Text = "ОК";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // структурнаяЕдиницаTableAdapter
            // 
            this.структурнаяЕдиницаTableAdapter.ClearBeforeFill = true;
            // 
            // типСтруктурнойЕдиницыTableAdapter
            // 
            this.типСтруктурнойЕдиницыTableAdapter.ClearBeforeFill = true;
            // 
            // сотрудникTableAdapter
            // 
            this.сотрудникTableAdapter.ClearBeforeFill = true;
            // 
            // TerritoriesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 472);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "TerritoriesForm";
            this.Text = "Территории";
            this.Load += new System.EventHandler(this.TerritoriesForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.типСтруктурнойЕдиницыBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tuberStatisticDBDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.структурнаяЕдиницаBindingSource)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.типСтруктурнойЕдиницыBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.сотрудникBindingSource1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.BindingSource типСтруктурнойЕдиницыBindingSource1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.BindingSource сотрудникBindingSource1;
        private TuberStatisticDBDataSet tuberStatisticDBDataSet;
        private System.Windows.Forms.BindingSource структурнаяЕдиницаBindingSource;
        private TuberStatisticDBDataSetTableAdapters.СтруктурнаяЕдиницаTableAdapter структурнаяЕдиницаTableAdapter;
        private System.Windows.Forms.BindingSource типСтруктурнойЕдиницыBindingSource;
        private TuberStatisticDBDataSetTableAdapters.ТипСтруктурнойЕдиницыTableAdapter типСтруктурнойЕдиницыTableAdapter;
        private System.Windows.Forms.BindingSource сотрудникBindingSource;
        private TuberStatisticDBDataSetTableAdapters.СотрудникTableAdapter сотрудникTableAdapter;
        private System.Windows.Forms.DataGridViewTextBoxColumn кодDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn наименованиеDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодТипСтруктурнойЕдиницыDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewComboBoxColumn кодСотрудникDataGridViewTextBoxColumn;
    }
}